package com.juego.dianrie.mapas

import android.graphics.Bitmap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

class Marcador(var marcador: Marker) {
    //Los tipos pueden ser enemigo mascota edificio
    var nombre = ""
    var idOtroPersonaje = ""
    var nivel = 1
    var imagen = R.drawable.personaje
    var imagenBitmap: Bitmap? = null
    var imagenBitmap2: Bitmap? = null
    //var imagenBitmapInvertida: Bitmap? = null
    var mirarIzquierda = false
    var cambiarPosicionImagen = true
    var vida = 100
    var maximaVida = 100
    var ataque = 10
    var distanciaAtaque = 10
    var puntuacion = 0
    var muerto = false
   // var velocidad = 0.00001
    var velocidad = 50
    var seguirPersonaje = false
    var irAPosicion = LatLng(0.0,0.0)
    var sizeIcon = 100


}