package com.juego.dianrie.mapas

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.location.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import android.os.AsyncTask
import android.widget.Toast
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.WindowManager
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.Circle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.parse.*
import kotlinx.android.synthetic.main.activity_rol.*
import kotlin.random.Random

class RolActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnMapClickListener {

    //variable comun con el codigo de permiso
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        var objecIdUsuario: ParseObject? = null
        var puntos: ParseObject? = null
        var personaje: Marcador? = null
        var mundo: ParseObject? = null

    }

    //sound Pool
    private lateinit var soundManager: SoundManager

    //Marcadores
    private var currentUser: FirebaseUser? = null
    private var enemigos: MutableList<Marcador?> = mutableListOf()
    private var respawnArray: MutableList<Marcador?> = mutableListOf()
    private var objetosArray: MutableList<Marcador?> = mutableListOf()
    private var centrosArray: MutableList<Marcador?> = mutableListOf()
    private var polilineasArray: MutableList<Polyline?> = mutableListOf()
    //private var amigosArray: MutableList<Marcador?> = mutableListOf()
    private var energiaArray: MutableList<Marcador?> = mutableListOf()
    private var ubicacionesArray: MutableList<Ubicaciones?> = mutableListOf()
    private var ruinasArray: MutableList<Marcador?> = mutableListOf()
    // private var ruinasArray: MutableList<Marcador?> = mutableListOf()
    private var otrosPersonajes: MutableList<Marcador?> = mutableListOf()
    private var mascotas: MutableList<Marcador?> = mutableListOf(null, null, null, null)
    //private var mascotasGuardadas: MutableList<Marcador?> = mutableListOf()
    private var pokeball: MutableList<Marcador?> = mutableListOf(null, null, null, null)
    private var irAPosicion: Marker? = null
    private var seguirAMarcador: Marker? = null
    private var edificios: MutableList<Marcador?> = mutableListOf()
    private var primeraVez = true
    private var errorBaseDatos = false
    private var referenciaParaRefresco: LatLng? = null
    //private var referenciaGenerarEdificiosApi: LatLng? = null
    private lateinit var circle: Circle
    private lateinit var circle2: Circle
    private lateinit var circleAtaque: Circle
    private var myLocation: LatLng? = null
    private var radioAccion = 2000
    //private var RADIO_CONSERVACION_MARCADORES = 500
   // private var radioRegenerarPersonaje = 2000
    private var radioMascotaPersiguirAEnemigos = 3000
    private var distanciaEdificiosOcultar = 10
    private var radioMascotaPersonaje = 50
    private var distanciaEnemigoPersiguePersonaje = 300
    private var tamanoMaximoImagen = 200
    private var tamanoMinimoImagen = 75
   // private var DISTANCIA_ENEMIGO_ALEJAR_DE_EDIFICIOS = 500
    private var distanciaSaltos = 0.00003
    private var ciclos = 0
    private var gpsMode = false
    private var multijugador = false
    private var pause = false
    //private var ultima = false
    private var findeljuego = false
    private var oleada = 1
    private var tareaAsincronaArray = mutableListOf<AsyncTask<String, Void, String>>()


    // [START declare_auth]
    private lateinit var mAuth: FirebaseAuth
    // [END declare_auth]

    private lateinit var mMap: GoogleMap
    //Creamos la variable del tipo correcto para tener la localizacion del dispositivo
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rol)
        //Evitamos que la pantalla se apague y el dispositivo se bloque
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        //sound Manager
        soundManager = SoundManager(this)
        soundManager.initSound()

        //Comprobamos si hay conexion a internet0
        Toast.makeText(
            this,
            "MUNDO: " + mundo!!.getString("nivel") + " No permitas que destruyan la ciudad",
            Toast.LENGTH_LONG
        ).show()
        //Toast.makeText(this, "MUNDO" + mundo!!.getString("nivel"), Toast.LENGTH_LONG).show()

        if (!compruebaConexion(this)) {
            Toast.makeText(this, "No tiene conexión a internet", Toast.LENGTH_LONG).show()
            finish()
        }

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance()
        // [END initialize_auth]

        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.currentUser
        if (currentUser == null) {
            val intent = Intent(this, FirebaseUIActivity::class.java)
            startActivity(intent)
            // signInAnonymously()
        } else {
            //Toast.makeText(this, "Usuario: $currentUser", Toast.LENGTH_LONG).show()
        }

        //Cargamos los datos de la base de datos de parse
        cargarDatosParse()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        //Se inizializa la variable con nuestra localizacion
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        //Configuramos las propiedades el Mapa
        mMap = googleMap
        mMap.setOnMapClickListener(this)
        mMap.setOnMarkerClickListener(this)
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = true
        mMap.isBuildingsEnabled = true
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        //gpsBoton.alpha = 0.5F
        atacarBoton.alpha = 0.5F
        multijugadorBoton.alpha = 0.0F

        circle = crearCirculo()
        circle2 = crearCirculo()
        circleAtaque = crearCirculo()


        //se controla que es lo que pasa al pulsar en los Botones
        pokeball1Boton.setOnClickListener {
            if (mascotas[0] != null) {
                pokeball(0)

            }
            pokeball(0)
        }
        //pokeball2.setOnClickListener { pokeball(1) }
        // pokeball3.setOnClickListener { pokeball(2) }
        // pokeball4.setOnClickListener { pokeball(3) }

        //Pulsando el atacar mustro las caracteristicas del arma equipada
        atacarBoton.setOnClickListener {
            Toast.makeText(this, "Ataque:${personaje!!.ataque} Rango:${personaje!!.distanciaAtaque}", Toast.LENGTH_LONG)
                .show()
        }
        pauseBoton.setOnClickListener {
            //Paro todas las tareas asincronas que se esten enjecutando y parando la consulta a la base de datos
            if (!pause) {
                //pauseBoton.alpha = 0.5F
                pauseBoton.setImageResource(R.drawable.play)
                pause = true
                findeljuego = true
            } else {
                //Activo el modo pause activando la tarea asincrona MoverPersonajes
                //pauseBoton.alpha = 1F
                pauseBoton.setImageResource(R.drawable.pausa)
                pause = false
                findeljuego = false
                MoverPersonajes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            }
        }

        multijugadorBoton.setOnClickListener {

        }

        //pulsando el boton del mapa cambio el tipo de mapa
        mapaBoton.setOnClickListener {
            if (mMap.mapType == GoogleMap.MAP_TYPE_NORMAL) {
                mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
            } else {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            }
        }
        //Pulsando el boton gps muevo la camara a la posicion del personaje
        gpsBoton.setOnClickListener {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(personaje!!.marcador.position, 14f))
        }



        setUpMap()

    }


    /**
     * Funcion que pasa una mascota de un array llamado mascotas a otro array llamado pokeball
     * tambien cambia las propiedades de el Marcador guardado ocultandolo o mostrandolo segun  sea necesario
     */
    private fun pokeball(index: Int) {
        if (mascotas[index] != null || pokeball[index] != null) {
            //sale de la pokeball
            if (mascotas[index] == null) {
                if (pokeball[index]!!.vida <= 0) {
                    //Toast.makeText(applicationContext, "Drone reparado ",Toast.LENGTH_SHORT).show()
                }
                mascotas[index] = pokeball[index]
                mascotas[index]!!.marcador.zIndex = 1F

                val masCercano = masCercanoDelArray(personaje!!.marcador.position, edificios)
                if (masCercano != null) {
                    val distanciaAEdificioCercano = distanciaA(
                        masCercano.marcador.position,
                        mascotas[index]!!.marcador.position
                    )
                    if (distanciaAEdificioCercano > distanciaEdificiosOcultar) {
                        mascotas[index]!!.marcador.isVisible = !mascotas[index]!!.marcador.isVisible
                    }
                }
                mascotas[index]!!.marcador.position = referenciaParaRefresco
                mascotas[index]!!.seguirPersonaje = true
                when (index) {
                    0 -> pokeball1Boton.setImageResource(R.drawable.pokebolaabierta)
                    //1 -> pokeball2.setImageResource(R.drawable.pokebolaabierta)
                    //2 -> pokeball3.setImageResource(R.drawable.pokebolaabierta)
                    //3 -> pokeball4.setImageResource(R.drawable.pokebolaabierta)
                    else -> println("Number too high")
                }
                pokeball[index] = null

                soundManager.playSound(2)
            } else {
                //Entra en la pokeball
                when (index) {
                    0 -> pokeball1Boton.setImageBitmap(mascotas[index]!!.imagenBitmap)
                    //1 -> pokeball2.setImageResource(mascotas[index]!!.imagen)
                    //2 -> pokeball3.setImageResource(mascotas[index]!!.imagen)
                    //3 -> pokeball4.setImageResource(mascotas[index]!!.imagen)
                    else -> println("Number too high")
                }
                mascotas[index]!!.marcador.isVisible = false
                pokeball[index] = mascotas[index]
                mascotas[index] = null
            }
        }
    }

    private fun setUpMap() {
        //Solicitamos permiso para acceder a la localización en tiempo de ejecución
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        // Creamos una referencia a system Location Manager que luego utlizaremos
        // para comprobar si el GPS esta activado  y para adquirir la ultima ubicacion conocida
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
        if (gpsMode) {
            activarGps(locationManager)
            //Activamos el punto azul que indica nuestra ubicacion actual
            mMap.isMyLocationEnabled = true
            //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(40.345594, -3.823388), 15f))
        } else {
            mMap.isMyLocationEnabled = false
        }


        /**
         * Se mueve el mapa a la ultima ubicacion conocida esto lo hace cuando abrimos la aplicacion
         */
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (primeraVez) {
                if (location != null) {

                    if (gpsMode) {
                        circle.center = LatLng(location.latitude, location.longitude)
                        circle.isVisible = true
                    }
                    //iniciarJuego(LatLng(location.latitude, location.longitude))

                }
            }
        }

    }

    /**
     * Funcion que muestra una alerta pidiendo que se active el GPS si no esta activado
     */
    private fun activarGps(locationManager: LocationManager) {
        val estadoGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (estadoGPS) {
            //El gps esta activado
        } else {
            val alerta = AlertDialog.Builder(this)
            alerta.setTitle("GPS Desactivado")
            alerta.setMessage("Es necesario activar el Gps Para poder usar esta aplicacion")
            alerta.setPositiveButton("YES") { _, _ ->
                // Do something when user press the positive button
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            // Display a negative button on alert dialog
            alerta.setNegativeButton("No") { _, _ ->
                //Toast.makeText(applicationContext,"You are not agree.",Toast.LENGTH_SHORT).show()
                finish()
            }
            alerta.show()
        }

    }

    // Define a listener that responds to location updates
    // Este codigo se ejucutrara siempre que cambiemos de posicion dandonos la nueva posicion
    private val locationListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            myLocation = LatLng(location.latitude, location.longitude)
            if (primeraVez) {
                //iniciarJuego(myLocation!!)
            }



        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    //override fun onMarkerClick(p0: Marker?) = false
    override fun onMarkerClick(p0: Marker?): Boolean {

            if (p0 != null) {
                irAPosicion?.remove()
                irAPosicion = null
                seguirAMarcador = marcadorMasCercano(p0.position)!!.marcador
                //irAPosicion(p0.position)
            }


        return true
    }

    //Cuando se toque el mapa se creara un punto para que el personaje se mueva a ese punto
    override fun onMapClick(p0: LatLng?) {
        //se limita la distancia a la que puede moverse el personaje desde la posicion del gps

            seguirAMarcador = null
            irAPosicion(p0)

    }


    private fun moverPersonaje(posicionInicial: LatLng, posicionFinal: LatLng): LatLng {

        personaje!!.marcador.isVisible = true
        circleAtaque.isVisible = true
        //circleAtaque.strokeColor = Color.argb(50, 255, 0, 0)
        circleAtaque.radius = personaje!!.distanciaAtaque.toDouble()
       // atacarBoton.alpha = 1F

        /**
         * Si la distancia al punto de control es mayor de 1000 se generan nuevos edificios y destruyo los que estan mas lejos
         * quitando el icono
         * tambien destruyo los enemigos lejanos
         */

        //Cuando el personaje que se esta moviendo llege a la posicion final dejara de moverse se para

        if (seguirAMarcador != null) {
            if (distanciaA(posicionInicial, posicionFinal) < 10) {
                seguirAMarcador = null
            }
        }

        if (distanciaA(posicionInicial, posicionFinal) < 10) {
                irAPosicion?.remove()
                irAPosicion = null
        }


        // Evaluamos si esta cerca de un edificio
        if (edificios.count() != 0) {

            for (edificio in edificios) {
                if (edificio != null && edificio.marcador.isVisible) {
                    if (distanciaA(posicionInicial, edificio.marcador.position) < distanciaEdificiosOcultar) {
                        //personaje!!.marcador.isVisible = false
                        //circleAtaque.isVisible = false
                        //atacarBoton.alpha = 0.5F
                        progressBarBoton.progress += 100
                        personaje!!.marcador.snippet = "vida: ${progressBarBoton.progress}%"
                        seguirAMarcador = masCercanoDelArray(personaje!!.marcador.position, enemigos)?.marcador
                        //val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.ambulancia)
                        if (personaje!!.imagenBitmap2 != null) {
                            val imagenFinal = Bitmap.createScaledBitmap(personaje!!.imagenBitmap2!!, 100, 100, false)
                            personaje!!.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                            personaje!!.imagenBitmap = personaje!!.imagenBitmap2
                            personaje!!.imagenBitmap2 = null
                        }
                        soundManager.playSound(1)

                    }
                }
            }
        }
        // Evaluamos si esta cerca de un objeto
        if (objetosArray.count() != 0) {

            for (objeto in objetosArray) {
                if (objeto != null && objeto.marcador.isVisible) {
                    if (distanciaA(posicionFinal, objeto.marcador.position) < 300) {
                        seguirAMarcador = masCercanoDelArray(personaje!!.marcador.position, enemigos)?.marcador
                        atacarBoton.setImageBitmap(objeto.imagenBitmap)
                        progressBarBoton.max = objeto.vida
                        progressBarBoton.progress = progressBarBoton.max
                        personaje!!.ataque += objeto.ataque
                        /*
                        if (personaje!!.ataque > 300) {
                            personaje!!.ataque = 300
                        }
                        */
                        personaje!!.distanciaAtaque += objeto.distanciaAtaque
                        /*
                        if (personaje!!.distanciaAtaque > 500) {
                            personaje!!.distanciaAtaque = 500
                        }
                        */
                        personaje!!.velocidad = objeto.velocidad

                        circleAtaque.radius = personaje!!.distanciaAtaque.toDouble()
                        Toast.makeText(this, "Encontraste: ${objeto.nombre}", Toast.LENGTH_LONG).show()
                        Toast.makeText(
                            this,
                            "Ataque:${personaje!!.ataque} Rango:${personaje!!.distanciaAtaque}",
                            Toast.LENGTH_LONG
                        )
                            .show()
                        //objeto.vida = 0
                        if (personaje!!.imagenBitmap2 != null){
                            personaje!!.imagenBitmap = personaje!!.imagenBitmap2
                        }
                        objeto.marcador.isVisible = false
                    }
                }
            }

        }

        //Evaluamos en que direccion y cuanto se va a mover y lo movemos a las nuevas coordenadas
        val coordenadas = coordenadasDestino(personaje!!, posicionFinal)
        circleAtaque.center = coordenadas
        return coordenadas
    }


    private fun moverMascota(mascota: Marcador, posicionFinal: LatLng, velocidad: Int): LatLng {
        //Evaluo segun la velocidad si tengo que moverlo en este ciclo
        if (!moverPorCiclo(velocidad)) {
            return mascota.marcador.position
        }

        mascota.marcador.isVisible = true
        mascota.marcador.zIndex = 2f

        val posicionInicial = mascota.marcador.position

        //Evaluamos si esta cerca de un enemigo
        if (enemigos.count() != 0) {
            val enemigoCercano = masCercanoDelArray(mascota.marcador.position, enemigos)
            if (enemigoCercano != null) {
                if (distanciaA(mascota.marcador.position, enemigoCercano.marcador.position) < mascota.distanciaAtaque) {

                    enemigoCercano.vida -= mascota.ataque

                    //Toast.makeText(this, "enemigo vida: -10", Toast.LENGTH_LONG).show()
                    enemigoCercano.marcador.snippet = "vida: ${enemigoCercano.vida}%"

                    //Cambiamos el tamaño de la imagen del enemigo segun se reduzga su vida
                    enemigoCercano.sizeIcon -= 10
                    if (enemigoCercano.sizeIcon < tamanoMinimoImagen) {
                        enemigoCercano.sizeIcon = tamanoMinimoImagen
                    }
                    val imagenFinal = Bitmap.createScaledBitmap(
                        enemigoCercano.imagenBitmap!!,
                        enemigoCercano.sizeIcon,
                        enemigoCercano.sizeIcon,
                        false
                    )
                    enemigoCercano.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                    //enemigoCercano.imagenBitmap = imagenFinal

                    //Si le quitamos vida al enemigo se aleja de la mascota
                    enemigoCercano.marcador.position =
                            LatLng(posicionInicial.latitude + 0.0009, posicionInicial.longitude + 0.0009)
                    soundManager.playSound(6)

                }
                //Si la vida del enemigo es meror o igual a 0 lo elimino y creo energia
                if (enemigoCercano.vida <= 0) {
                    val energia = crearMarcador(enemigoCercano.marcador.position, "Energia", R.drawable.atomo)
                    energia.puntuacion = enemigoCercano.puntuacion
                    energiaArray.add(energia)
                    personaje!!.puntuacion += enemigoCercano.puntuacion
                    //Toast.makeText(applicationContext, "${enemigoCercano.puntuacion} Puntos Más", Toast.LENGTH_SHORT ).show()
                    puntuacionTextView.text = personaje!!.puntuacion.toString()
                    ejecutarRespawn(enemigoCercano)
                    enemigoCercano.marcador.remove()
                    enemigos.remove(enemigoCercano)
                    soundManager.playSound(4)
                }
            }
        }

        // Evaluamos si esta cerca de un edificio si es un hospital se cura
        if (edificios.count() != 0) {
            for (edificio in edificios) {
                if (edificio != null && edificio.marcador.isVisible) {

                    if (distanciaA(posicionInicial, edificio.marcador.position) < distanciaEdificiosOcultar) {
                        mascota.marcador.isVisible = false

                        if (mascota.vida < 100) {
                            mascota.vida += 100
                            mascota.sizeIcon += 100
                            if (mascota.sizeIcon > tamanoMaximoImagen) {
                                mascota.sizeIcon = tamanoMaximoImagen
                            }
                            mascotas.indexOf(mascota)
                            when (mascotas.indexOf(mascota)) {
                                0 -> pokeball1Boton.alpha = 1F
                                // 1 -> pokeball2.alpha = 1F
                                // 2 -> pokeball3.alpha = 1F
                                // 3 -> pokeball4.alpha = 1F
                                else -> println("Number too high")
                            }
                            Toast.makeText(
                                applicationContext,
                                "${mascota.nombre} vida: ${mascota.vida}%",
                                Toast.LENGTH_SHORT
                            ).show()
                            soundManager.playSound(1)
                        }

                    }
                }
            }
        }
//Evaluamos en que direccion y cuanto se va a mover y lo movemos a las nuevas coordenadas
        return coordenadasDestino(mascota, posicionFinal)
    }

    private fun moverEnemigo(enemigo: Marcador, posicionFinal: LatLng, velocidad: Int): LatLng {
        enemigo.marcador.isVisible = true
        //enemigo.marcador.zIndex = 2f
        val posicionInicial = enemigo.marcador.position

        if (!moverPorCiclo(velocidad)) {
            return enemigo.marcador.position
        }

        //Evaluo si esta cerca del personaje para aplicarle el daño lo hago en la siguiente
        if (distanciaA(
                posicionInicial,
                personaje!!.marcador.position
            ) < enemigo.distanciaAtaque && personaje!!.marcador.isVisible
        ) {
            if (progressBarBoton.progress <= 0) {
                // Toast.makeText(this, "Estás Muerto", Toast.LENGTH_LONG).show()
            } else {
                progressBarBoton.progress = progressBarBoton.progress - enemigo.ataque
            }
            //Si le quitamos vida al personaje se alaja del enemigo

            // personaje!!.marcador.position = LatLng(posicionInicial.latitude + 0.0001, posicionInicial.longitude + 0.0001)


            // soundManager.playSound(1)
        }
        if (distanciaA(
                posicionInicial,
                personaje!!.marcador.position
            ) < personaje!!.distanciaAtaque && personaje!!.marcador.isVisible
        ) {

            //Evaluo que daño se le aplica al enemigo por enfrentarse al personaje
            if (circleAtaque.isVisible) {
                if (enemigo.vida > 0) {
                    enemigo.vida -= personaje!!.ataque

                    //Toast.makeText(this, "enemigo vida: -10", Toast.LENGTH_LONG).show()
                    enemigo.marcador.snippet = "vida: ${enemigo.vida}% ataque: ${enemigo.ataque}%"
                    //Cambiamos el tamaño de la imagen del enemigo segun se reduzga su vida
                    //var imagen = enemigo.vida
                    enemigo.sizeIcon -= 10
                    if (enemigo.sizeIcon < tamanoMinimoImagen) {
                        enemigo.sizeIcon = tamanoMinimoImagen
                    }
                    val imagenFinal =
                        Bitmap.createScaledBitmap(enemigo.imagenBitmap!!, enemigo.sizeIcon, enemigo.sizeIcon, false)
                    enemigo.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                    //enemigo.imagenBitmap = imagenFinal

                    if (enemigo.vida > 0) {
                        seguirAMarcador = enemigo.marcador
                    } else {
                        seguirAMarcador = null
                        irAPosicion?.remove()
                        irAPosicion = null
                        enemigo.marcador.isVisible = false
                    }

                    personaje!!.marcador.snippet =
                            "vida: ${progressBarBoton.progress}% ataque: ${personaje!!.ataque}%"
                    if (personaje!!.distanciaAtaque > 10) {
                        personaje!!.distanciaAtaque -= enemigo.distanciaAtaque

                        if (personaje!!.distanciaAtaque < 10) {
                            personaje!!.distanciaAtaque = 10
                            circleAtaque.radius = personaje!!.distanciaAtaque.toDouble()
                        }

                    } else {
                        //atacarBoton.setImageResource(R.drawable.puno)
                        for (objeto in objetosArray) {
                            if (!objeto!!.marcador.isVisible) {
                                objeto.marcador.position =
                                        posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!)
                                objeto.marcador.isVisible = true

                                val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.paquete)
                                val imagenFinal1 = Bitmap.createScaledBitmap(imagenOriginal, 100, 100, false)
                                objeto.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal1))
                                objeto.irAPosicion = LatLng(
                                    objeto.marcador.position.latitude - 0.01,
                                    objeto.marcador.position.longitude + 0.01
                                )

                            }
                        }

                    }


                }
                //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la derechea o a la izquierda
                if (personaje!!.mirarIzquierda) {
                    enemigo.marcador.position = LatLng(posicionInicial.latitude, posicionInicial.longitude - 0.001)
                    personaje!!.marcador.position = LatLng(
                        personaje!!.marcador.position.latitude,
                        personaje!!.marcador.position.longitude + 0.001
                    )

                } else {
                    enemigo.marcador.position = LatLng(posicionInicial.latitude, posicionInicial.longitude + 0.001)
                    personaje!!.marcador.position = LatLng(
                        personaje!!.marcador.position.latitude,
                        personaje!!.marcador.position.longitude - 0.001
                    )
                }
                if (enemigo.marcador.position.latitude > personaje!!.marcador.position.latitude) {
                    enemigo.marcador.position =
                            LatLng(posicionInicial.latitude + 0.0001, enemigo.marcador.position.longitude)
                    personaje!!.marcador.position = LatLng(
                        personaje!!.marcador.position.latitude - 0.0001,
                        personaje!!.marcador.position.longitude
                    )
                } else {
                    enemigo.marcador.position =
                            LatLng(posicionInicial.latitude - 0.0001, enemigo.marcador.position.longitude)
                    personaje!!.marcador.position = LatLng(
                        personaje!!.marcador.position.latitude + 0.0001,
                        personaje!!.marcador.position.longitude
                    )
                }
                circleAtaque.center = personaje!!.marcador.position
                soundManager.playSound(0)

            }
        }
        //Evaluamos si esta cerca del circulo del campo de fuerza de la torre central
        if (distanciaA(enemigo.marcador.position, referenciaParaRefresco!!) < circle.radius) {
            //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la derechea o a la izquierda
            if (enemigo.mirarIzquierda) {
                enemigo.marcador.position = LatLng(posicionInicial.latitude, posicionInicial.longitude + 0.001)

            } else {
                enemigo.marcador.position = LatLng(posicionInicial.latitude, posicionInicial.longitude - 0.001)
            }
            //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la derechea o a la izquierda
            if (enemigo.marcador.position.latitude > referenciaParaRefresco!!.latitude) {
                enemigo.marcador.position =
                        LatLng(posicionInicial.latitude + 0.001, enemigo.marcador.position.longitude)

            } else {
                enemigo.marcador.position =
                        LatLng(posicionInicial.latitude - 0.001, enemigo.marcador.position.longitude)

            }
            if (circle.radius < 10) {
                circle.radius = 0.0
            } else {
                if (enemigo.ataque >= circle.radius) {
                    circle.radius = 0.0
                } else {
                    circle.radius -= enemigo.ataque
                }

            }
            //soundManager.playSound(0)

        }

        // Evaluamos si esta cerca de un edificio y alejado mas de distanciaEdificiosOcultar
        if (edificios.count() != 0) {
            for (edificio in edificios) {
                if (edificio != null && edificio.marcador.isVisible) {
                    if (distanciaA(posicionInicial, edificio.marcador.position) < distanciaEdificiosOcultar) {
                        if (edificio.vida > 0) {
                            edificio.vida -= personaje!!.ataque
                            //Toast.makeText(this, "enemigo vida: -10", Toast.LENGTH_LONG).show()
                            edificio.marcador.snippet = "vida: ${edificio.vida}%"
                            soundManager.playSound(0)
                        }
                        enemigo.marcador.position =
                                LatLng(posicionInicial.latitude - 0.001, posicionInicial.longitude - 0.001)
                    }
                }
            }
        }

        //Comprobamos si hay alguna mascota cerca y aplicamos el daño
        if (mascotas.count() != 0) {
            val mascotaCercana = masCercanoDelArray(enemigo.marcador.position, mascotas)

            if (mascotaCercana != null) {
                if (distanciaA(enemigo.marcador.position, mascotaCercana.marcador.position) < enemigo.distanciaAtaque) {
                    if (mascotaCercana.vida <= 0) {
                        //mascotaCercana.seguirPersonaje = false
                        mascotaCercana.ataque += 10
                        when (mascotas.indexOf(mascotaCercana)) {
                            0 -> {
                                pokeball1Boton.setImageBitmap(mascotaCercana.imagenBitmap)
                                pokeball1Boton.alpha = 0.5F

                                pokeball(0)
                                pokeball(0)
                            }

                            1 -> {

                                pokeball(1)
                                pokeball(1)

                            }
                            2 -> {

                                pokeball(2)
                                pokeball(2)
                            }
                            3 -> {

                                pokeball(3)
                                pokeball(3)
                            }
                            else -> println("Number too high")
                        }


                        //Toast.makeText(this, "mascota esta muerta", Toast.LENGTH_LONG).show()
                    } else {
                        mascotaCercana.vida -= enemigo.ataque
                        soundManager.playSound(0)
                        //Toast.makeText(this, "mascota vida: -10", Toast.LENGTH_LONG).show()
                        mascotaCercana.marcador.snippet =
                                "vida: ${mascotaCercana.vida}% ataque: ${mascotaCercana.ataque}%"
                        //Cambiamos el tamaño de la imagen del enemigo segun se reduzga su vida
                        mascotaCercana.sizeIcon = mascotaCercana.vida

                        if (mascotaCercana.sizeIcon > tamanoMaximoImagen) {
                            mascotaCercana.sizeIcon = tamanoMaximoImagen
                        } else if (mascotaCercana.sizeIcon < tamanoMinimoImagen) {
                            mascotaCercana.sizeIcon = tamanoMinimoImagen
                        }
                        val imagenFinal = Bitmap.createScaledBitmap(
                            mascotaCercana.imagenBitmap!!,
                            mascotaCercana.sizeIcon,
                            mascotaCercana.sizeIcon,
                            false
                        )
                        mascotaCercana.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))

                        //Si le quitamos vida al enemigo se alaja del personaje controlamos si es a la drechea o a la izquierda
                        if (mascotaCercana.mirarIzquierda) {
                            enemigo.marcador.position =
                                    LatLng(posicionInicial.latitude, posicionInicial.longitude - 0.0003)
                            mascotaCercana.marcador.position = LatLng(
                                mascotaCercana.marcador.position.latitude,
                                mascotaCercana.marcador.position.longitude + 0.0003
                            )

                        } else {
                            enemigo.marcador.position =
                                    LatLng(posicionInicial.latitude, posicionInicial.longitude + 0.0003)
                            mascotaCercana.marcador.position = LatLng(
                                mascotaCercana.marcador.position.latitude,
                                mascotaCercana.marcador.position.longitude - 0.0003
                            )

                        }
                    }

                    //Si le quitamos vida a la mascota se alaja del enemigo
                    // mascotaCercana.marcador.position = LatLng(posicionInicial.latitude - 0.0009, posicionInicial.longitude - 0.0009)


                }
            }
        }

        //Evaluamos en que direccion y cuanto se va a mover y lo movemos a las nuevas coordenadas
        return coordenadasDestino(enemigo, posicionFinal)

    }

    //Evaluamos en que direccion y cuanto se va a mover y lo movemos a las nuevas coordenadas
    private fun coordenadasDestino(marcador: Marcador, posicionFinal: LatLng): LatLng {
        var posicion: LatLng
        val posicionInicial = marcador.marcador.position


        posicion = if (posicionInicial.longitude < posicionFinal.longitude) {
            //Si el marcador se despalza a la derecha pongo su icono mirando a la derecha

            if (marcador.mirarIzquierda) {
                if (marcador.cambiarPosicionImagen) {
                    if (marcador.imagenBitmap != null) {

                        val imagenFinal = Bitmap.createScaledBitmap(
                            marcador.imagenBitmap!!,
                            marcador.sizeIcon,
                            marcador.sizeIcon,
                            false
                        )
                        marcador.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal!!))
                    }
                    marcador.mirarIzquierda = false
                    marcador.cambiarPosicionImagen = false
                } else {
                    marcador.cambiarPosicionImagen = true
                }
            }

            LatLng(posicionInicial.latitude, posicionInicial.longitude + distanciaSaltos)
        } else {
            //Si el marcador se despalza a la izquierda pongo su icono mirando a la izquierda
            if (!marcador.mirarIzquierda) {
                if (marcador.imagenBitmap != null) {

                    val imagenFinal =
                        Bitmap.createScaledBitmap(marcador.imagenBitmap!!, marcador.sizeIcon, marcador.sizeIcon, false)

                    val flippedBitmap = imagenFinal!!.flip()
                    marcador.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(flippedBitmap))
                    marcador.mirarIzquierda = true
                    marcador.cambiarPosicionImagen = false
                }

            }
            marcador.cambiarPosicionImagen = false
            LatLng(posicionInicial.latitude, posicionInicial.longitude - distanciaSaltos)
        }

        posicion = if (posicionInicial.latitude < posicionFinal.latitude) {
            LatLng(posicion.latitude + distanciaSaltos, posicion.longitude)
        } else {
            LatLng(posicion.latitude - distanciaSaltos, posicion.longitude)
        }

        return posicion
    }


    /**
     * // Tarea asincrona que mueve los marcadores cada cierto tiempo
     */

    @SuppressLint("StaticFieldLeak")
    private inner class MoverPersonajes : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg p0: String?): String {

            //Obligatorio
            var contador = 0
            while (contador <= 1) {
                try {
                    // Thread.sleep(5)
                    contador++
                    // Log.i("contador", "Bucle 25 millis: $contador")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            //Log.i("contador","ciclo: $ciclos")
            ciclos += 1
            if (ciclos >= 100) {
                ciclos = 0
            }
            return "ciclo realizado"
        }

        override fun onPostExecute(result: String?) {
            if (!findeljuego) {


                if (progressBarBoton.progress == 0) {
                    if (edificios.count() != 0) {
                        irAPosicion(masCercanoDelArray(personaje!!.marcador.position, edificios)!!.marcador.position)
                        //personaje!!.marcador.isVisible = false
                        if (personaje!!.imagenBitmap2 == null) {
                            personaje!!.imagenBitmap2 = personaje!!.imagenBitmap
                            val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.ambulancia)
                            val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 100, 100, false)
                            personaje!!.imagenBitmap = imagenOriginal
                            personaje!!.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))

                        }

                    }
                } else {
                    //Aqui es donde se controla como se mueve el personaje en cada ciclo
                    if (seguirAMarcador != null) {
                        personaje!!.marcador.position =
                                moverPersonaje(
                                    personaje!!.marcador.position,
                                    seguirAMarcador!!.position
                                )
                        //irAPosicion(seguirAMarcador!!.position)
                    }

                }
                if (irAPosicion != null) {
                    personaje!!.marcador.position =
                            moverPersonaje(personaje!!.marcador.position, irAPosicion!!.position)
                }


                //Aqui es donde se controla como se mueven las mascotas en cada ciclo
                if (mascotas.count() != 0) {
                    for (mascota in mascotas) {
                        if (mascota != null) {
                            //Evaluamos lo lejos estan los enemigos para perseguirlos o ir donde esta el personaje
                            val enemigoMasCercano = masCercanoDelArray(mascota.marcador.position, enemigos)
                            val distancia = distanciaA(mascota.marcador.position, personaje!!.marcador.position)

                            //la mascota se detiene a 50 metros del personaje
                            if (distancia < 50) {
                                if (!mascota.seguirPersonaje) {
                                    mascota.seguirPersonaje = true
                                    mascota.vida = 100
                                    mascota.marcador.alpha = 1F
                                    Toast.makeText(this@RolActivity, "Drone Reparado", Toast.LENGTH_LONG).show()
                                    soundManager.playSound(2)
                                }
                            }


                            //Si esta activado segur personaje compruebo si hay enemigos cerca para seguirlos
                            if (mascota.seguirPersonaje) {
                                if (enemigoMasCercano != null) {
                                    if (distanciaA(
                                            personaje!!.marcador.position,
                                            enemigoMasCercano.marcador.position
                                        ) < radioMascotaPersiguirAEnemigos
                                    ) {
                                        var posicionElementoMasCercano: Marcador? = null

                                        // Evaluamos que elemento del array esta mas cerca
                                        if (mascotas.count() != 0) {
                                            var distancia1 = 30000

                                            for (elemento in mascotas) {
                                                if (elemento != null && elemento.marcador.isVisible && elemento != mascota) {

                                                    if (distanciaA(
                                                            enemigoMasCercano.marcador.position,
                                                            elemento.marcador.position
                                                        ) < distancia1 && elemento.marcador.isVisible
                                                    ) {
                                                        posicionElementoMasCercano = elemento
                                                        distancia1 = distanciaA(
                                                            enemigoMasCercano.marcador.position,
                                                            elemento.marcador.position
                                                        )
                                                    }
                                                }
                                            }
                                        }
                                        //Controlamos que la mascota siga a el enemigo mas cercano que no tenga una mascota cerca
                                        if (posicionElementoMasCercano != null) {
                                            if (distanciaA(
                                                    enemigoMasCercano.marcador.position,
                                                    posicionElementoMasCercano.marcador.position
                                                ) < 100
                                            ) {
                                                var seguirAlMasCercano = true
                                                var seguirAEnemigo: Marcador? = null

                                                for (enemigo in enemigos) {
                                                    val mascotaMasCercana =
                                                        masCercanoDelArray(enemigo!!.marcador.position, mascotas)
                                                    if (mascotaMasCercana != null) {
                                                        if (distanciaA(
                                                                enemigo.marcador.position,
                                                                mascotaMasCercana.marcador.position
                                                            ) > 100
                                                        ) {
                                                            seguirAEnemigo = enemigo
                                                            seguirAlMasCercano = false
                                                            break
                                                        }

                                                    }

                                                }
                                                //Si todos los enemigos tiene una mascota cerca iremos donde el enemigo mas cercano
                                                if (seguirAlMasCercano) {
                                                    mascota.marcador.position = moverMascota(
                                                        mascota, enemigoMasCercano.marcador.position,
                                                        mascota.velocidad
                                                    )
                                                }else{
                                                    mascota.marcador.position = moverMascota(
                                                        mascota, seguirAEnemigo!!.marcador.position,
                                                        mascota.velocidad
                                                    )
                                                }

                                            } else {
                                                mascota.marcador.position = moverMascota(
                                                    mascota, enemigoMasCercano.marcador.position,
                                                    mascota.velocidad
                                                )
                                            }
                                        } else {
                                            mascota.marcador.position = moverMascota(
                                                mascota, enemigoMasCercano.marcador.position,
                                                mascota.velocidad
                                            )
                                        }
                                        //Si no hay enemigos cerca muevo la mascota hacia el personaje
                                    } else {
                                        if (distancia > radioMascotaPersonaje) {
                                            mascota.marcador.position =
                                                    moverMascota(
                                                        mascota,
                                                        personaje!!.marcador.position,
                                                        mascota.velocidad
                                                    )
                                        }
                                    }
                                } else {
                                    if (distancia > radioMascotaPersonaje) {
                                        mascota.marcador.position =
                                                moverMascota(mascota, personaje!!.marcador.position, mascota.velocidad)
                                    }
                                }
                            }
                        }
                    }
                }

                //Consulto las nuevas actualizaciones de la base de datos
                //Como las posiciones de los otros personajes


                //Aqui es donde se controla como se mueven los enemigos en cada ciclo
                if (enemigos.count() != 0) {

                    var eliminarEnemigo: Marcador? = null
                    for (enemigo in enemigos) {
                        if (enemigo != null) {


                            if (enemigo.vida <= 0) {
                                eliminarEnemigo = enemigo
                                enemigo.marcador.isVisible = false

                            } else {
                                //Evaluo segun la velocidad si tengo que moverlo en este ciclo
                                //Evaluamos lo lejos que esta el personaje para perseguirlo o ir un edificio cercano
                                val edificioMasCercano = masCercanoDelArray(enemigo.marcador.position, edificios)
                                if (personaje!!.marcador.isVisible && distanciaA(
                                        enemigo.marcador.position,
                                        personaje!!.marcador.position
                                    ) < distanciaEnemigoPersiguePersonaje
                                ) {
                                    enemigo.marcador.position =
                                            moverEnemigo(enemigo, personaje!!.marcador.position, enemigo.velocidad)
                                } else {
                                    if (edificioMasCercano != null) {
                                        enemigo.marcador.position = moverEnemigo(
                                            enemigo,
                                            edificioMasCercano.marcador.position,
                                            enemigo.velocidad
                                        )
                                    }
                                }

                            }
                        }

                    }
                    //Si la vida del enemigo es menor o igual a 0 lo eliminamos
                    if (eliminarEnemigo != null) {
                        /*
                        if (distanciaSaltos < 0.0005) {
                            distanciaSaltos = 0.00003
                        }
                        */
                        val energia = crearMarcador(eliminarEnemigo.marcador.position, "Energia", R.drawable.atomo)
                        energia.puntuacion = eliminarEnemigo.puntuacion
                        val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.atomo)
                        val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 50, 50, false)
                        energia.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))

                        energiaArray.add(energia)

                        personaje!!.puntuacion += eliminarEnemigo.puntuacion
                        //Toast.makeText(applicationContext, "${eliminarEnemigo.puntuacion} Puntos Más", Toast.LENGTH_SHORT).show()
                        puntuacionTextView.text = personaje!!.puntuacion.toString()
                        ejecutarRespawn(eliminarEnemigo)
                        enemigos.remove(eliminarEnemigo)
                        soundManager.playSound(4)
                        seguirAMarcador = masCercanoDelArray(personaje!!.marcador.position, enemigos)?.marcador


                    }
                }


                //Aqui es donde se controla el movimiento de la energia
                if (energiaArray.count() != 0) {
                    var eliminarEnergia: Marcador? = null
                    for (energia in energiaArray) {
                        if (energia != null) {

                            energia.marcador.position = coordenadasDestino(energia, referenciaParaRefresco!!)

                            if (distanciaA(energia.marcador.position, referenciaParaRefresco!!) <= 10) {
                                eliminarEnergia = energia
                            }
                        }
                    }
                    //Si eliminamos el marcador energia
                    if (eliminarEnergia != null) {
                        eliminarEnergia.marcador.remove()
                        energiaArray.remove(eliminarEnergia)
                        if (circle.radius < 1000) {
                            val size = (circle.radius / 50).toInt() + 100
                            val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.casa)
                            val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, size, size, false)
                            edificios[0]!!.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                        } else {
                            val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.casa)
                            val imagenFinal = Bitmap.createScaledBitmap(
                                imagenOriginal,
                                (circle.radius / 10).toInt(),
                                (circle.radius / 10).toInt(),
                                false
                            )
                            edificios[0]!!.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                        }
                        circle.radius += eliminarEnergia.puntuacion
                        if (circle.radius > 2000) {
                            finDelJuego(true, "Ganaste la ciudad esta protegida")
                        }
                        soundManager.playSound(5)

                    }
                }
                //Aqui es donde se controla el movimiento del paquete de objeto
                //Aqui es donde se controla cuando se eliminan los objetos

                if (objetosArray.count() != 0) {
                    var eliminarObjeto: Marcador? = null

                    for (objeto in objetosArray) {

                        if (objeto != null && objeto.marcador.isVisible) {
                            if (objeto.irAPosicion.latitude != 0.0) {

                                objeto.marcador.position = coordenadasDestino(objeto, objeto.irAPosicion)

                                if (distanciaA(objeto.marcador.position, objeto.irAPosicion) <= 10) {
                                    objeto.irAPosicion = LatLng(0.0, 0.0)
                                    val imagenFinal = Bitmap.createScaledBitmap(objeto.imagenBitmap!!, 75, 75, false)
                                    objeto.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                                }
                            }

                            if (objeto.vida <= 0) {
                                eliminarObjeto = objeto
                            }
                        }

                    }


                    //Si la vida del objeto es menor o igual a 0 lo eliminamos
                    if (eliminarObjeto != null) {
                        eliminarObjeto.marcador.isVisible = false
                        objetosArray.remove(eliminarObjeto)
                        soundManager.playSound(5)

                    }

                }

                //Aqui es donde se controla cuando se eliminan los edificios
                if (edificios.count() != 0) {
                    var elimiarEdificio: Marcador? = null
                    var edificiosVisibles = false
                    for (edificio in edificios) {
                        if (edificio != null) {
                            if (edificio.marcador.isVisible) {
                                edificiosVisibles = true
                            }
                            if (edificio.vida <= 0) {

                                //edificio.marcador.title = "ruinas"
                                if (edificios.count() == 1) {
                                    // Toast.makeText(applicationContext, "Todos los edificios fueron destruidos", Toast.LENGTH_LONG).show()
                                    finDelJuego(false, "Todos los edificios han sido destruidos")
                                } else {
                                    elimiarEdificio = edificio
                                }

                            }
                        }
                    }
                    if (!edificiosVisibles) {
                        if (centrosArray.count() > 0) {
                            edificios.add(0, crearMarcador(centrosArray.last()!!.marcador.position, centrosArray.last()!!.nombre,R.drawable.casa))
                            referenciaParaRefresco = edificios[0]!!.marcador.position
                            circle.center = referenciaParaRefresco
                            circle.radius = 1000.0
                            personaje!!.marcador.position = referenciaParaRefresco
                            circleAtaque.center = referenciaParaRefresco
                            centrosArray.last()!!.marcador.remove()
                            centrosArray.remove(centrosArray.last())
                            if (polilineasArray.count() > 0) {
                                polilineasArray.last()!!.isVisible = false
                                polilineasArray.remove(polilineasArray.last())
                            }
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(referenciaParaRefresco, 14f))
                            //Toast.makeText(this, "Un centro ha sido destruido", Toast.LENGTH_LONG).show()
                        }else {
                            finDelJuego(false, "La ciduda ${edificios[0]?.nombre} ha sido destruida")
                            findeljuego = true
                        }
                    }

                    //Si la vida del edificio es menor o igual a 0 lo eliminamos
                    if (elimiarEdificio != null) {
                        ruinasArray.add(crearMarcador(elimiarEdificio.marcador.position, "Ruina", R.drawable.ruina))
                        elimiarEdificio.vida = 100
                        elimiarEdificio.marcador.isVisible = false
                        //edificios.remove(elimiarEdificio)
                        soundManager.playSound(3)
                    }
                }

                MoverPersonajes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                super.onPostExecute(result)
            }
        }

    }

    /**
     * // Tarea asincrona que Consulta la base de datos cada 60Segundos
     */

    @SuppressLint("StaticFieldLeak")
    private inner class ConsultarDatosParse : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg p0: String?): String {

            //Obligatorio
            try {
                Thread.sleep(60000)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            //Log.i("contador","ciclo: $ciclos")
            return "no cancelado"
        }

        override fun onPostExecute(result: String?) {

            //Consulto en la base de datos la tabla MundosUsuario y evaluo donde se encuentran los otros usuarios

            val query = ParseQuery.getQuery<ParseObject>("MundosUsuarios")
            val queryUsuarios = ParseQuery.getQuery<ParseObject>("Usuarios")
            query.whereContains("objectIdMundo", mundo!!.getString("nivel"))
            query.findInBackground { listaConsulta, e ->
                if (e == null) {
                    for (actualizacion in listaConsulta) {

                        //Si el registro es del tipo usuario actualizo su posicion en el mapa
                        if (actualizacion.getString("tipo") == "usuario") {
                            var crear = true
                            for (otroPersonaje in otrosPersonajes) {
                                if (otroPersonaje != null) {
                                    if (actualizacion.getString("objectIdUsuario") == otroPersonaje.idOtroPersonaje) {
                                        otroPersonaje.irAPosicion =
                                                LatLng(
                                                    actualizacion.getDouble("lat"),
                                                    actualizacion.getDouble("lng")
                                                )
                                        crear = false
                                    }

                                }
                            }
                            if (actualizacion.getString("objectIdUsuario") == objecIdUsuario!!.objectId) {
                                //Actualizo en la base de datos la posicion del personaje
                                actualizacion.put("lat", personaje!!.marcador.position.latitude)
                                actualizacion.put("lng", personaje!!.marcador.position.longitude)
                                actualizacion.saveInBackground()

                                crear = false
                            }
                            //Creo el otro personaje si no existe
                            //Compruebo si existe en el array de otrospersonajes y si existe
                            //Pongo la variable crear a false y no creo el otroUsuario

                            if (crear) {
                                val otroPersonaje2 = crearMarcador(
                                    LatLng(actualizacion.getDouble("lat"), actualizacion.getDouble("lng")),
                                    actualizacion.getString("nombre").toString(),
                                    R.drawable.otropersonaje
                                )

                                otroPersonaje2.velocidad = actualizacion.getInt("velocidad")
                                otroPersonaje2.marcador.isVisible = true
                                //Busco su icono y lo añado al marcador
                                queryUsuarios.whereEqualTo("objectId", actualizacion.getString("objectIdUsuario"))
                                queryUsuarios.findInBackground { listaUsuarios1, error ->
                                    if (error == null && listaUsuarios1.size > 0) {
                                        //saco la imagen del personaje de la base de datos y la meto en su marcador
                                        val applicantResume = listaUsuarios1[0].getParseFile("avatar")
                                        applicantResume?.getDataInBackground { data, e ->
                                            if (e == null) {
                                                // data has the bytes for the resume
                                                //val imageBytes = Base64.decode(data,0)
                                                val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                                otroPersonaje2.imagenBitmap = bmp
                                                //val imagenOriginal = BitmapFactory.decodeResource(resources, usuario.getInt("imagen"))
                                                val imagenFinal =
                                                    Bitmap.createScaledBitmap(bmp, 100, 100, false)
                                                otroPersonaje2.marcador.setIcon(
                                                    BitmapDescriptorFactory.fromBitmap(
                                                        imagenFinal
                                                    )
                                                )

                                            } else {
                                                // something went wrong
                                            }
                                        }

                                    }
                                }
                                otroPersonaje2.idOtroPersonaje = actualizacion.getString("objectIdUsuario")!!
                                otrosPersonajes.add(otroPersonaje2)
                                //personaje!!.marcador.zIndex = 10000F
                                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordenadasInicio, 15f))
                            }


                        }
                        /**
                         *  Si el registro es del tipo enemigo actualizo su posicion en el mapa
                          */


                        if (actualizacion.getString("tipo") == "") {
                            //Compruebo si existe en el array de enemigos y si existe
                            //Pongo la variable crear a false y no creo
                            var crear = true
                            for (enemigo in enemigos) {
                                if (enemigo != null) {
                                    if (actualizacion.objectId == enemigo.idOtroPersonaje) {
                                        enemigo.vida = actualizacion.getInt("vida")
                                        crear = false
                                    }
                                }
                            }

                            //Creo el enemigo si no existe
                            if (crear) {

                                val enemigo2 = crearMarcador(
                                    LatLng(actualizacion.getDouble("lat"), actualizacion.getDouble("lng")),
                                    actualizacion.getString("nombre").toString(),
                                    R.drawable.zombie
                                )
                                //saco la imagen del enemigo de la base de datos y la meto en el marcador
                                val applicantResume = actualizacion.getParseFile("icono")
                                applicantResume?.getDataInBackground { data, error ->
                                    if (error == null) {
                                        val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                        enemigo2.imagenBitmap = bmp
                                        val imagenFinal = Bitmap.createScaledBitmap(bmp, 100, 100, false)
                                        enemigo2.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                                    } else {
                                        // something went wrong
                                    }
                                }



                                enemigo2.idOtroPersonaje = actualizacion.objectId
                                enemigo2.velocidad = actualizacion.getInt("velocidad")
                                enemigo2.ataque = actualizacion.getInt("ataque")
                                enemigo2.vida = actualizacion.getInt("vida")
                                enemigo2.nivel = actualizacion.getInt("nivel")
                                enemigo2.marcador.isVisible = true
                                //lo añado al array para que se comporte como un enemigo
                                enemigos.add(enemigo2)
                            }
                        }
                        /**
                         * Si el registro es del tipo edificio actualizo su posicion en el mapa
                          */

                        if (actualizacion.getString("tipo") == "tesoro") {

                        }
                    }
                } else {
                    Log.d("Tag", "Mover otroUsuario: " + e.message)
                }
            }

            tareaAsincronaArray.add(ConsultarDatosParse().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR))


            super.onPostExecute(result)
        }

    }

    private fun iniciarJuego(coordenadasInicio: LatLng) {

        if (primeraVez) {
            referenciaParaRefresco = coordenadasInicio
            //Creo el personaje principal
            personaje = crearMarcador(coordenadasInicio, currentUser!!.displayName!!, R.drawable.personaje)
            personaje!!.imagen = R.drawable.personaje
            var imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.personaje)
            personaje!!.imagenBitmap = imagenOriginal
            personaje!!.velocidad = 100
            personaje!!.marcador.isVisible = false
            personaje!!.marcador.zIndex = 2f
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordenadasInicio, 14f))
            circle.center = referenciaParaRefresco
            circle.radius = 100.0
            circle.isVisible = true

            circleAtaque.center = coordenadasInicio
            circleAtaque.isVisible = false
            circleAtaque.strokeColor = Color.argb(50, 255, 0, 0)
            circleAtaque.radius = personaje!!.distanciaAtaque.toDouble()
            atacarBoton.alpha = 1F

            //Creo la Casa
            val casa = crearMarcador(
                coordenadasInicio, "", R.drawable.casa
            )
            casa.marcador.zIndex = 2f
            imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.casa)
            val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 100, 100, false)
            casa.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))

            edificios.add(0, casa)

            //Inicio el movimiento de los personajes

            MoverPersonajes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            if (multijugador) {
                tareaAsincronaArray.add(ConsultarDatosParse().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR))
            }
            //primeraVez = false
        }


    }

    private fun irAPosicion(p0: LatLng?) {

        if (irAPosicion == null) {
            val markerOption = MarkerOptions()
                .position(p0!!)
                .title("Mover Aqui")
                .draggable(true)
                .visible(true)
                .zIndex(0f)
            val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.iraposicion3)
            val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 50, 50, false)

            markerOption.icon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
            irAPosicion = mMap.addMarker(markerOption)

        } else {
            //
            irAPosicion!!.position = p0

        }

    }

    private fun crearMarcador(posicion: LatLng, nombre: String, imagen: Int): Marcador {
        val markerOption = MarkerOptions()
        val imagenOriginal = BitmapFactory.decodeResource(resources, imagen)
        val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 100, 100, false)

        markerOption
            .position(posicion)
            // .title(nombre)
            .draggable(true)
            .visible(true)
        markerOption.icon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
        val marcador = Marcador(mMap.addMarker(markerOption))
        marcador.nombre = nombre
        return marcador
    }

    /**
     * // Tarea asincrona que busca una localizacion dada una palabra clave
     * y coloca un marcador en el mapa si encuentra alguna coincidencia
     */
    @SuppressLint("StaticFieldLeak")
    internal inner class GetPlaces(
        var context: Context, private var localizacionActual: LatLng
    ) :
        AsyncTask<Void, Void, Void>() {

        private var localizacionesEncontradas: ArrayList<Ubicaciones?>? = null

        override fun doInBackground(vararg params: Void?): Void? {
            localizacionesEncontradas = obtenerLocalizacionesDeGoogle(localizacionActual)
            return null
        }

        private fun obtenerLocalizacionesDeGoogle(localizacion: LatLng?): ArrayList<Ubicaciones?>? {
            val servicio = PlaceServices()
            return servicio.buscarRestaurantes(localizacion!!.latitude, localizacion.longitude)
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            ubicacionesArray = localizacionesEncontradas!!
            if (oleada >= 5) {
                oleada = 4
            }


        }

    }

    /**
     * Funcion devuelve el elemento Marcador de un array que este mas cerca de
     * las coordenadas facilitadas como posicionInicial
     */
    private fun masCercanoDelArray(posicionInicial: LatLng, array: MutableList<Marcador?>): Marcador? {
        var posicionElementoMasCercano: Marcador? = null

        // Evaluamos que elemento del array esta mas cerca
        if (array.count() != 0) {
            var distancia = 30000

            for (elemento in array) {
                if (elemento != null && elemento.marcador.isVisible) {

                    if (distanciaA(
                            posicionInicial,
                            elemento.marcador.position
                        ) < distancia && elemento.marcador.isVisible
                    ) {
                        posicionElementoMasCercano = elemento
                        distancia = distanciaA(posicionInicial, elemento.marcador.position)
                    }
                }
            }
        }
        return posicionElementoMasCercano

    }

    /**
     * // Funcion devuelve el elemento Marcador que este mas cerca de
     * las coordenadas facilitadas como posicionInicial
     */
    private fun marcadorMasCercano(posicionInicial: LatLng): Marcador? {
        var posicionElementoMasCercano: Marcador? = null
        val arrayDeArraysDeMarcadores: MutableList<MutableList<Marcador?>> = mutableListOf()
        arrayDeArraysDeMarcadores.add(edificios)
        arrayDeArraysDeMarcadores.add(enemigos)
        arrayDeArraysDeMarcadores.add(respawnArray)
        arrayDeArraysDeMarcadores.add(otrosPersonajes)
        arrayDeArraysDeMarcadores.add(mascotas)
        arrayDeArraysDeMarcadores.add(objetosArray)
        // Evaluamos que elemento del array esta mas cerca

        if (arrayDeArraysDeMarcadores.count() != 0) {
            var distancia = 30000
            for (array in arrayDeArraysDeMarcadores) {
                if (array.count() != 0) {
                    for (elemento in array) {
                        if (elemento != null) {

                            if (distanciaA(
                                    posicionInicial,
                                    elemento.marcador.position
                                ) < distancia && elemento.marcador.isVisible
                            ) {
                                posicionElementoMasCercano = elemento
                                distancia = distanciaA(posicionInicial, elemento.marcador.position)
                            }
                        }
                    }
                }
            }
        }

        return posicionElementoMasCercano

    }

    /**
     * // Funcion devuelve la distancia entre dos puntos dadso como objetos LatLng
     */
    private fun distanciaA(miPosicion: LatLng, otroPosicion: LatLng): Int {
        val posicionInicialLocation = Location("")
        posicionInicialLocation.longitude = miPosicion.longitude
        posicionInicialLocation.latitude = miPosicion.latitude

        val otroPosicionLocation = Location("")
        otroPosicionLocation.longitude = otroPosicion.longitude
        otroPosicionLocation.latitude = otroPosicion.latitude

        return posicionInicialLocation.distanceTo(otroPosicionLocation).toInt()
    }

    /**
     * // Funcion que segun el numero que le pasemos como velocidad
     * calcula si en el ciclo actual corresponde mover o no mover, devuelbe un Bolean
     * se utiliza para saver si a en este ciclo toca mover a un personaje enemigo o amigo etc
     * y poder simular distintas velocidades segun la variable asignada al Marcador
     */

    private fun moverPorCiclo(velocidad: Int): Boolean {
        val num1 = (100 / velocidad)
        var mover = false
        var suma = num1
        while (suma <= 100) {
            if (suma == ciclos) {
                mover = true
            }
            suma += num1
        }
        return mover
    }

    private fun cargarDatosParse() {
        val query = ParseQuery.getQuery<ParseObject>("MundosUsuarios")
        val queryUsuarios = ParseQuery.getQuery<ParseObject>("Usuarios")
        query.whereContains("objectIdMundo", mundo!!.getString("nivel"))
        val querymascotas = ParseQuery.getQuery<ParseObject>("Mascotas")


        query.findInBackground { mundosUsuario, e ->
            if (e == null) {
                errorBaseDatos = false
                //Creo el personaje principal consultando la base de datos

                referenciaParaRefresco = LatLng(
                    puntos!!.getDouble("lat"),
                    puntos!!.getDouble("lng")
                )

                if (primeraVez) {
                    //Creo el personaje principal
                    iniciarJuego(
                        referenciaParaRefresco!!
                    )
                    //Creo  Un array con los ciudades cercanas consultando la API de sitios cercanos
                    val getPlaces = GetPlaces(
                        this@RolActivity, referenciaParaRefresco!!
                    )
                    getPlaces.execute()
                }
                for (registroMundosUsuario in mundosUsuario) {
                    if (registroMundosUsuario.getString("tipo") == "usuario" && primeraVez) {
                                                val idUsuario = registroMundosUsuario.getString("objectIdUsuario")
                        if (idUsuario == objecIdUsuario!!.objectId) {
                            primeraVez = false

                            //saco la imagen del arma de la base de datos y la meto en su boton
                            val applicantResume = registroMundosUsuario.getParseFile("icono")
                            applicantResume?.getDataInBackground { data, error ->
                                if (error == null) {
                                    val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                    val imagenFinal = Bitmap.createScaledBitmap(bmp, 100, 100, false)
                                    atacarBoton.setImageBitmap(imagenFinal)
                                } else {
                                    // something went wrong
                                }
                            }
                            // Cargo las caracteristicas del personaje en este mundo concreto
                            personaje!!.vida = registroMundosUsuario.getInt("vida")
                            personaje!!.nivel = registroMundosUsuario.getInt("nivel")
                            personaje!!.ataque = registroMundosUsuario.getInt("ataque")
                            personaje!!.velocidad = registroMundosUsuario.getInt("velocidad")
                            personaje!!.distanciaAtaque = registroMundosUsuario.getInt("distanciaAtaque")
                            personaje!!.puntuacion = registroMundosUsuario.getInt("puntuacion")
                            circleAtaque.radius = registroMundosUsuario.getInt("distanciaAtaque").toDouble()


                            // Cargo el avatar del personaje
                            // Creo las mas cotas que estan guadadas en la base de datos
                            if (personaje != null && (personaje!!.idOtroPersonaje == "1" || personaje!!.idOtroPersonaje == "")) {
                                personaje!!.idOtroPersonaje = objecIdUsuario!!.objectId

                                queryUsuarios.whereEqualTo("objectId", objecIdUsuario!!.objectId)
                                queryUsuarios.findInBackground { listaUsuarios1, error ->
                                    if (error == null) {
                                        //saco la imagen del personaje de la base de datos y la meto en su marcador
                                        val applicantResume1 = listaUsuarios1[0].getParseFile("avatar")
                                        applicantResume1?.getDataInBackground { data, e ->
                                            if (e == null) {
                                                val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                                val imagenFinal = Bitmap.createScaledBitmap(bmp, 100, 100, false)
                                                personaje!!.imagenBitmap = bmp
                                                personaje!!.marcador.setIcon(
                                                    BitmapDescriptorFactory.fromBitmap(imagenFinal)
                                                )

                                            } else {
                                                // something went wrong
                                            }
                                        }


                                    }
                                }

                                querymascotas.whereEqualTo("objectIdUsuario", objecIdUsuario!!.objectId)
                                //Consulto en la base de datos las mascotas asociadas al personaje y las cargo
                                querymascotas.findInBackground { listaMascotas, error ->
                                    if (error == null) {
                                        var index = 0

                                        for (mascota in listaMascotas) {
                                            if (mascota.getString("nombre") != "") {
                                                val applicantResume2 = mascota.getParseFile("icono")
                                                if (applicantResume2 != null) {
                                                    val marcador = crearMarcador(
                                                        referenciaParaRefresco!!,
                                                        mascota.getString("nombre").toString(),
                                                        R.drawable.androide
                                                    )
                                                    marcador.marcador.zIndex = 2F
                                                    marcador.velocidad = mascota.getInt("velocidad")
                                                    marcador.vida = mascota.getInt("vida")
                                                    marcador.ataque = mascota.getInt("ataque")
                                                    marcador.imagen = R.drawable.androide
                                                    marcador.marcador.isVisible = true
                                                    marcador.seguirPersonaje = true

                                                    marcador.nivel = mascota.getInt("nivel")
                                                    pokeball[index] = marcador

                                                    applicantResume2.getDataInBackground { data, e ->
                                                        if (e == null) {

                                                            val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                                            val imagenFinal =
                                                                Bitmap.createScaledBitmap(bmp, 100, 100, false)
                                                            marcador.imagenBitmap = bmp
                                                            marcador.marcador.setIcon(
                                                                BitmapDescriptorFactory.fromBitmap(
                                                                    imagenFinal
                                                                )
                                                            )
                                                            pokeball(mascota.getInt("posicion"))
                                                        } else {
                                                            // something went wrong
                                                            Log.i("mascota", e.message)
                                                        }


                                                    }

                                                } else {
                                                    pokeball[index] = null
                                                }
                                                mascotas[index] = null
                                                index += 1
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }


                    /**
                     * Si el registro es del tipo enemigo actualizo su posicion en el mapa
                      */


                    if (registroMundosUsuario.getString("tipo") == "enemigo") {
                        //Creo el respawn de enemigos si no existe
                            //Se crea un marcador para el respawn

                            val respawn2 = crearMarcador(
                                posicionAleatoriaRelativaEnemigo(referenciaParaRefresco!!), "Portal", R.drawable.portal
                            )

                            //saco la imagen del enemigo de la base de datos y la meto en el marcador
                            val applicantResume = registroMundosUsuario.getParseFile("icono")
                            applicantResume?.getDataInBackground { data, error ->
                                if (error == null) {
                                    val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                    respawn2.imagenBitmap = bmp
                                    respawn2.idOtroPersonaje = registroMundosUsuario.objectId
                                    respawn2.velocidad = registroMundosUsuario.getInt("velocidad")
                                    respawn2.ataque = registroMundosUsuario.getInt("ataque")
                                    respawn2.vida = registroMundosUsuario.getInt("vida")
                                    respawn2.nivel = registroMundosUsuario.getInt("nivel")
                                    respawn2.nombre = registroMundosUsuario.getString("nombre").toString()
                                    respawn2.puntuacion = registroMundosUsuario.getInt("puntuacion")
                                    respawn2.marcador.isVisible = true
                                    //lo añado al array para que se comporte como un respawn
                                    respawnArray.add(respawn2)

                                    if (oleada + 2 > respawnArray.count()) {
                                        ejecutarRespawn(respawn2)

                                        seguirAMarcador =
                                                masCercanoDelArray(personaje!!.marcador.position, enemigos)?.marcador
                                    } else {
                                        respawn2.marcador.isVisible = false

                                    }
                                } else {
                                    // something went wrong
                                }
                            }


                    }
                    /**
                     * Si el registro es del tipo edificio actualizo su posicion
                      */


                    if (registroMundosUsuario.getString("tipo") == "edificio") {
                        //Compruebo si existe en el array de edificios y si existe
                        //Pongo la variable crear a false y no creo

                        //Creo el edificio

                            //Se crea un marcador para el edificio

                            val edificio2 = crearMarcador(
                                posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!),
                                registroMundosUsuario.getString("nombre").toString(),
                                R.drawable.edificio
                            )
                            //saco la imagen del edificio de la base de datos y la meto en el marcador
                            val applicantResume = registroMundosUsuario.getParseFile("icono")
                            applicantResume?.getDataInBackground { data, error ->
                                if (error == null) {
                                    val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                    val imagenFinal = Bitmap.createScaledBitmap(bmp, 120, 120, false)
                                    //edificio2.marcador.zIndex = 2f
                                    edificio2.imagenBitmap = bmp
                                    edificio2.imagenBitmap = imagenFinal
                                    edificio2.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                                    edificio2.idOtroPersonaje = registroMundosUsuario.objectId
                                    edificio2.velocidad = registroMundosUsuario.getInt("velocidad")
                                    edificio2.ataque = registroMundosUsuario.getInt("ataque")
                                    edificio2.vida = registroMundosUsuario.getInt("vida")
                                    edificio2.nivel = registroMundosUsuario.getInt("nivel")
                                    edificio2.nombre = registroMundosUsuario.getString("nombre").toString()
                                    edificio2.marcador.isVisible = true
                                    //lo añado al array para que se comporte como un respawn
                                    edificios.add(edificio2)

                                } else {
                                    // something went wrong
                                }
                            }


                    }
                    /**
                     * Si el registro es del tipo objeto actualizo su posicion
                     */
                    if (registroMundosUsuario.getString("tipo") == "objeto") {

                            //Se crea un marcador para el objeto

                            val objeto2 = crearMarcador(
                                posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!),
                                registroMundosUsuario.getString("nombre").toString(),
                                R.drawable.paquete
                            )

                            val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.paquete)
                            val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 75, 75, false)
                            objeto2.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                            //saco la imagen del edificio de la base de datos y la meto en el marcador
                            val applicantResume = registroMundosUsuario.getParseFile("icono")
                            applicantResume?.getDataInBackground { data, error ->
                                if (error == null) {
                                    val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                    //val imagenFinal2 = Bitmap.createScaledBitmap(bmp, 75, 75, false)
                                    objeto2.marcador.zIndex = 2f
                                    objeto2.imagenBitmap = bmp
                                    //objeto2.imagenBitmap = imagenFinal2
                                    //objeto2.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal2))
                                    objeto2.idOtroPersonaje = registroMundosUsuario.objectId
                                    objeto2.velocidad = registroMundosUsuario.getInt("velocidad")
                                    objeto2.ataque = registroMundosUsuario.getInt("ataque")
                                    objeto2.vida = registroMundosUsuario.getInt("vida")
                                    objeto2.nivel = registroMundosUsuario.getInt("nivel")
                                    objeto2.distanciaAtaque = registroMundosUsuario.getInt("distanciaAtaque")
                                    objeto2.nombre = registroMundosUsuario.getString("nombre").toString()
                                    objeto2.marcador.isVisible = true
                                    objeto2.marcador.zIndex = 4000f
                                    objeto2.irAPosicion = LatLng(
                                        objeto2.marcador.position.latitude - 0.01,
                                        objeto2.marcador.position.longitude + 0.01
                                    )
                                    //lo añado al array para que se comporte como un respawn
                                    objetosArray.add(objeto2)

                                } else {
                                    // something went wrong
                                }
                            }



                    }
                }

            } else {
                Log.d("Usuarios", "Error: " + e.message)
                //Comprobamos si hay conexion a internet0
                if (!compruebaConexion(this)) {
                    Toast.makeText(this, "No tiene conexión a internet", Toast.LENGTH_LONG).show()
                    finish()
                }
                Toast.makeText(this, "Error base de datos: " + e.message, Toast.LENGTH_LONG).show()
                errorBaseDatos = true
            }
        }
    }
//Si le pasamos un respawn que es un marcador del respawnArray creara un enemigo en la localizacion del respawn
//Si lo pasamos null se ejecutan del array de respawn

    private fun ejecutarRespawn(enemigo1: Marcador?) {
        val respawn: Marcador?
        var index = respawnArray.indexOf(enemigo1)

        if (enemigo1 != null) {
            for (respawn1 in respawnArray) {
                if (enemigo1.idOtroPersonaje == respawn1!!.idOtroPersonaje) {
                    respawn1.marcador.isVisible = false

                }
            }
            for (enemigo in enemigos) {
                if (enemigo!!.idOtroPersonaje == enemigo1.idOtroPersonaje) {
                    index = Random.nextInt(0, respawnArray.count())
                }
            }
            respawn = respawnArray[index]
            respawn!!.marcador.isVisible = true
            respawn.vida += enemigo1.puntuacion
            respawn.puntuacion += respawn.vida

            respawn.marcador.position = posicionAleatoriaRelativaEnemigo(referenciaParaRefresco!!)

            if (respawn.velocidad >= 100) {
                respawn.velocidad = 100
            }
            if (respawn.puntuacion >= 100) {
                respawn.puntuacion = 100
            }


            if (respawn.nivel < 1) {
                respawn.marcador.isVisible = false
                respawnArray.remove(respawn)
                if (respawnArray.count() == 0) {
                    finDelJuego(true, "Ganaste mataste a todos los enemigos")
                }
            } else {

                val enemigo = crearMarcador(
                    respawn.marcador.position,
                    respawn.nombre,
                    R.drawable.zombie
                )
                //Se controla que el enemigo na sea mas grande de 200 x 200
                enemigo.sizeIcon = Random.nextInt(0, tamanoMaximoImagen)

                if (enemigo.sizeIcon < tamanoMinimoImagen) {
                    enemigo.sizeIcon = tamanoMinimoImagen
                }
                val imagenFinal =
                    Bitmap.createScaledBitmap(respawn.imagenBitmap!!, enemigo.sizeIcon, enemigo.sizeIcon, false)
                enemigo.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))
                enemigo.imagenBitmap = respawn.imagenBitmap

                enemigo.idOtroPersonaje = respawn.idOtroPersonaje
                enemigo.velocidad = respawn.velocidad
                enemigo.ataque = respawn.ataque
                enemigo.vida = respawn.vida
                enemigo.nivel = respawn.nivel
                enemigo.puntuacion = respawn.puntuacion
                enemigo.marcador.zIndex = 3000f
                enemigo.marcador.isVisible = true

                //lo añado al array para que se comporte como un enemigo
                enemigos.add(enemigo)
            }
        } else {
            for (respawn2 in respawnArray) {
                ejecutarRespawn(respawn2)
            }

        }
    }

    private fun finDelJuego(isWiner: Boolean, mensaje: String) {
        if (isWiner) {

            finDelJuego(false, "")

            oleada++
            if (distanciaSaltos < 0.00009) {
                distanciaSaltos += 0.00001

            }


            val bunker = crearMarcador(
                referenciaParaRefresco!!,
                "Protegida",
                R.drawable.casa
            )
            val imagenOriginal = BitmapFactory.decodeResource(resources, R.drawable.casa)
            val imagenFinal = Bitmap.createScaledBitmap(imagenOriginal, 150, 150, false)
            bunker.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal))

            bunker.marcador.zIndex = 2f
            val latLongPolylinea = referenciaParaRefresco!!


            if (ubicacionesArray.count() == oleada + 1) {
                //Creo  Un array con los ciudades cercanas consultando la API de sitios cercanos
                val getPlaces = GetPlaces(this@RolActivity, referenciaParaRefresco!!)
                getPlaces.execute()

            }
            //Si no se consigue encontrar nunguna ubicacion cercana se pondra de forma aleatoria
            referenciaParaRefresco = if (ubicacionesArray.count() > oleada){
                LatLng(ubicacionesArray[oleada]!!.latitud, ubicacionesArray[oleada]!!.longitud)
            }else{
                posicionAleatoriaRelativaEnemigo(referenciaParaRefresco!!)
            }
            centrosArray.add(bunker)

            val polilinea = crearPolilinea(
                latLongPolylinea,
                referenciaParaRefresco!!
            )
            polilineasArray.add(polilinea)

            for ((i, edificio) in edificios.withIndex()) {
                if (edificio != null) {
                    if (i == 0) {
                        val imagenOriginal1 = BitmapFactory.decodeResource(resources, R.drawable.casa)
                        val imagenFinal1 = Bitmap.createScaledBitmap(imagenOriginal1, 100, 100, false)
                        edificio.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal1))
                        edificio.marcador.position = referenciaParaRefresco!!
                        edificio.marcador.isVisible = true
                        edificio.vida = 100

                    } else {
                        edificio.marcador.position = posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!)
                        edificio.marcador.isVisible = true
                        edificio.vida = 100
                    }
                }
            }


            circle.radius = 100.0
            circle.center = referenciaParaRefresco
            circle.isVisible = true
            personaje!!.marcador.position = referenciaParaRefresco
            irAPosicion(referenciaParaRefresco!!)
            circleAtaque.center = personaje!!.marcador.position



            for (ruinas in ruinasArray) {
                ruinas!!.marcador.remove()
            }
            ruinasArray.removeAll(ruinasArray)

            if (personaje!!.distanciaAtaque < 100) {
                for (objeto in objetosArray) {
                    if (objeto != null) {
                        objeto.marcador.position = posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!)
                        objeto.marcador.isVisible = true
                        val imagenOriginal2 = BitmapFactory.decodeResource(resources, R.drawable.paquete)
                        val imagenFinal2 = Bitmap.createScaledBitmap(imagenOriginal2, 100, 100, false)
                        objeto.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(imagenFinal2))
                        objeto.irAPosicion =
                                LatLng(
                                    objeto.marcador.position.latitude - 0.01,
                                    objeto.marcador.position.longitude + 0.01
                                )
                    }
                }
            }

            //Creo las mascotas o amigos que ayudaran al personaje
            if (oleada < 5) {
                val velocidad = 50
                val arrayImagenes: Array<Int> = arrayOf(R.drawable.tank, R.drawable.eva, R.drawable.robotmanos)
                val i = oleada
                val marcador = crearMarcador(
                    posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!),
                    "robot",
                    arrayImagenes[i - 2]
                )
                marcador.marcador.zIndex = 3000f
                marcador.velocidad = velocidad
                marcador.marcador.isVisible = true
                //marcador.seguirPersonaje = true
                marcador.marcador.alpha = 0.5F
                marcador.imagen = arrayImagenes[i - 2]
                val imagenOriginal3 = BitmapFactory.decodeResource(resources, arrayImagenes[i - 2])
                val imagenFinal3 = Bitmap.createScaledBitmap(imagenOriginal3, 100, 100, false)
                marcador.imagenBitmap = imagenFinal3
                mascotas[i - 1] = marcador

            }
            for (mascota in mascotas) {
                if (mascota != null) {
                    mascota.marcador.position = posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!)
                }
            }
            for (energia in energiaArray) {
                if (energia != null) {
                    energia.marcador.position = posicionAleatoriaRelativaEdificio(referenciaParaRefresco!!)
                }
            }

            for (enemigo in enemigos) {
                enemigo!!.marcador.remove()
            }
            enemigos.removeAll(enemigos)

            for (respawn in respawnArray) {
                respawn!!.marcador.isVisible = false

            }

            for ((count, respawn) in respawnArray.withIndex()) {
                if (count < oleada + 1 && count <= 4) {
                    ejecutarRespawn(respawn)
                }
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(referenciaParaRefresco!!, 14f))
            seguirAMarcador = masCercanoDelArray(personaje!!.marcador.position, enemigos)?.marcador


        } else {

            val alerta = AlertDialog.Builder(this)
            if (mensaje != "") {
                alerta.setTitle(mensaje)
                alerta.setPositiveButton("Reintentar") { _, _ ->
                    recreate()
                    findeljuego = true
                                    }
            } else {
                alerta.setTitle("La ciudad ${edificios[0]!!.nombre} está protegida")
                if (ubicacionesArray.count() > oleada + 1){
                    alerta.setPositiveButton("Atacan ${ubicacionesArray[oleada + 1]?.nombre}") { _, _ ->
                        findeljuego = false
                        MoverPersonajes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                    }
                }else {
                    alerta.setPositiveButton("Atacan otra ubicación") { _, _ ->
                        findeljuego = false
                        MoverPersonajes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                    }
                }
            }
            //Guardo la maxima puntuacion si supera la que ya tenimos y la ubicacion de la ultima ciudad protegida
            if (puntos!!.getInt("puntuacion") < personaje!!.puntuacion) {
                puntos!!.put("puntuacion", personaje!!.puntuacion)
            }
            puntos!!.put("lng", edificios[0]!!.marcador.position.longitude)
            puntos!!.put("lat", edificios[0]!!.marcador.position.latitude)
            puntos!!.saveInBackground { }

            if (mundo!!.getInt("puntuacionRanking") < personaje!!.puntuacion) {
                mundo!!.put("puntuacionRanking", personaje!!.puntuacion)
                mundo!!.put("usuarioRanking", mAuth.currentUser!!.displayName!!)
                mundo!!.saveInBackground()
            }

            // Display a negative button on alert dialog
            alerta.setMessage(
                "Puntuación actual: ${personaje!!.puntuacion}\nRanking 1º: ${mundo!!.getString(
                    "usuarioRanking"
                )} ${mundo!!.getInt("puntuacionRanking")}"
            )


            if (puntos!!.getInt("puntuacion") > 1000 || personaje!!.puntuacion > 1000) {
                if (mundo!!.getString("nivel")!!.toInt() >= objecIdUsuario!!.getInt("nivel")) {
                    objecIdUsuario!!.put("nivel", mundo!!.getString("nivel")!!.toInt() + 1)
                    objecIdUsuario!!.saveInBackground()
                }
            }

            alerta.setNegativeButton("Menú") { _, _ ->
                soundManager.cleanUpIfEnd()
                startActivity(
                    Intent(this, SeleccionarMundoActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )
                finish()
            }


            try {
                alerta.show()
            } catch (e: WindowManager.BadTokenException) {
                //use a log message
            }

        }
    }
    private fun posicionAleatoriaRelativaEnemigo(posicion: LatLng): LatLng {
        val posicionFinal: LatLng
        val lat1 = Random.nextDouble(0.0, 0.019)
        val lng1 = Random.nextDouble(0.018, 0.020)
        val lat2 = Random.nextDouble(0.018, 0.020)
        val lng2 = Random.nextDouble(0.0, 0.019)
        posicionFinal = if (Random.nextBoolean()) {
            if (Random.nextBoolean()) {
                if (Random.nextBoolean()) {
                    LatLng(posicion.latitude + lat1, posicion.longitude + lng1)
                } else {
                    LatLng(posicion.latitude - lat1, posicion.longitude - lng1)
                }
            } else {
                if (Random.nextBoolean()) {
                    LatLng(posicion.latitude + lat1, posicion.longitude - lng1)
                } else {
                    LatLng(posicion.latitude - lat1, posicion.longitude + lng1)
                }
            }
        } else {
            if (Random.nextBoolean()) {
                if (Random.nextBoolean()) {
                    LatLng(posicion.latitude + lat2, posicion.longitude + lng2)
                } else {
                    LatLng(posicion.latitude - lat2, posicion.longitude - lng2)
                }
            } else {
                if (Random.nextBoolean()) {
                    LatLng(posicion.latitude + lat2, posicion.longitude - lng2)
                } else {
                    LatLng(posicion.latitude - lat2, posicion.longitude + lng2)
                }
            }
        }
        return posicionFinal
    }
    private fun posicionAleatoriaRelativaEdificio(posicion: LatLng): LatLng {
        val posicionFinal: LatLng
        val lat = Random.nextDouble(0.0001, 0.014)
        val lng = Random.nextDouble(0.0001, 0.014)
        posicionFinal = if (Random.nextBoolean()) {
            if (Random.nextBoolean()) {
                LatLng(posicion.latitude + lat, posicion.longitude + lng)
            } else {
                LatLng(posicion.latitude - lat, posicion.longitude - lng)
            }
        } else {
            if (Random.nextBoolean()) {
                LatLng(posicion.latitude + lat, posicion.longitude - lng)
            } else {
                LatLng(posicion.latitude - lat, posicion.longitude + lng)
            }
        }
        return posicionFinal
    }
    private fun compruebaConexion(context: Context): Boolean {
        var connected = false
        val connec = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        // Recupera todas las redes (tanto móviles como wifi)
        val redes = connec.allNetworkInfo
        for (i in redes.indices) {
            // Si alguna red tiene conexión, se devuelve true
            if (redes[i].state === NetworkInfo.State.CONNECTED) {
                connected = true
            }
        }
        return connected
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && (resultCode == Activity.RESULT_OK)) {
            // Si la respuesta coincide con el codigo y es ok
        } else {
            // Sign in failed. If response is null the user canceled the
        }

    }
    //Funcion que añade la capacidad de voltear una imagen
    private fun Bitmap.flip(): Bitmap {
        val matrix = Matrix().apply { postScale(-1f, 1f, width / 2f, width / 2f) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    override fun finish() {
        findeljuego = true
        soundManager.cleanUpIfEnd()
        super.finish()
    }
    private var backPressed: Long = 0
    override fun onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis())
            super.onBackPressed()
        else
            Toast.makeText(this, "Pulse otra vez para salir", Toast.LENGTH_SHORT).show()
        backPressed = System.currentTimeMillis()
    }
    private fun crearCirculo(): Circle {

        val circleOptions = CircleOptions()
            .center(LatLng(0.0, 0.0))
            .radius(radioAccion.toDouble()) // In meters
            .zIndex(1f)
            .strokeWidth(10F)
            .strokeColor(Color.argb(50, 0, 0, 255))
            .fillColor(Color.argb(10, 0, 0, 255))
            .clickable(false)
            .visible(false)
        return mMap.addCircle(circleOptions)
    }
    private fun crearPolilinea(puntoInicio: LatLng, PuntoFin: LatLng): Polyline {

        val polylineOptions = PolylineOptions()
            .add(puntoInicio, PuntoFin)
            .zIndex(1f)
            .color(Color.argb(50, 0, 0, 255))
            .clickable(false)
            .visible(true)
            .geodesic(true)

        val polyline = mMap.addPolyline(polylineOptions)
        polyline.jointType = JointType.ROUND
        return polyline

    }
}