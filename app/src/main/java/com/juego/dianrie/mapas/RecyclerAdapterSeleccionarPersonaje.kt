package com.juego.dianrie.mapas

import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.parse.ParseFile
import java.io.ByteArrayOutputStream


class RecyclerAdapterSeleccionarPersonaje(private var arrayAvatar: MutableList<Bitmap>) :
    RecyclerView.Adapter<RecyclerAdapterSeleccionarPersonaje.ViewHolder>() {
    //Data Source

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapterSeleccionarPersonaje.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout_personajes, p0, false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        return arrayAvatar.size
    }

    override fun onBindViewHolder(p0: RecyclerAdapterSeleccionarPersonaje.ViewHolder, p1: Int) {
        p0.mostrarImagenPersonaje.setImageBitmap(arrayAvatar[p1])
        p0.imagenPersonaje = arrayAvatar[p1]
        p0.tipo = "hola"

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mostrarImagenPersonaje: ImageView = itemView.findViewById(R.id.imageViewPersonaje)
        var imagenPersonaje: Bitmap? = null
        //var intent = Intent(itemView.context, MapsActivity::class.java)
        var tipo: String = ""

        init {

            //Celda Listener para pasar datos si se seleciona la celda
            mostrarImagenPersonaje.setOnClickListener {
                Log.i("celda", "Se ha seleccionado la celda")


                // Now let's update it with some new data. In this case, only cheatMode and score
                // will get sent to the Parse Cloud. playerName hasn't changed.

                //Meto la imagen seleccionada en la variable global del personaje para que aparezca al volver a la
                //Activity MapsActivity
                // val bitmap = Bitmap.createScaledBitmap(imagenPersonaje, 100, 100, false)
                // MapsActivity.personaje!!.marcador.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap))

                val stream = ByteArrayOutputStream()
                imagenPersonaje!!.compress(Bitmap.CompressFormat.PNG, 90, stream)
                val data = ParseFile("imagen.png", stream.toByteArray())

                //Guardamos la imagen en la base de datos parse en la tabla usuarios en el registro del usuario actual en el camop avatar
                SeleccionarMundoActivity.idUsuario!!.put("avatar", data)
                SeleccionarMundoActivity.idUsuario!!.saveInBackground()
                //Volvemos a la Activity anterior
                // (itemView.context as SelecionarPersonajeActivity).finish()
                itemView.context.startActivity(Intent(itemView.context, SeleccionarMundoActivity::class.java))


                //Extras
                //Toast.makeText(itemView.context,"Elige un tamaño y los extras que quieras",Toast.LENGTH_LONG).show()
                //intent.putExtra("Posicion", "${posicion}")
                //itemView.context.startActivity(intent)

            }

        }
    }
}