package com.juego.dianrie.mapas

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.parse.ParseFile
import com.parse.ParseObject
import com.parse.ParseQuery
import java.io.ByteArrayOutputStream


class RecyclerAdapterSeleccionarMundo(
    var arrayMundos: MutableList<ParseObject>,
    var idUsuario: ParseObject,
    var context: Context
) : RecyclerView.Adapter<RecyclerAdapterSeleccionarMundo.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerAdapterSeleccionarMundo.ViewHolder {
        val vistaCelda = LayoutInflater.from(p0.context).inflate(R.layout.card_layout_mundos, p0, false)
        return ViewHolder(vistaCelda)
    }

    override fun getItemCount(): Int {
        //Data Source
        return arrayMundos.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(p0: RecyclerAdapterSeleccionarMundo.ViewHolder, p1: Int) {
        p0.nombreMundo.text = "Nivel:${arrayMundos[p1].getString("nivel")} ${arrayMundos[p1].getString("nombre")}"
        p0.nombreRanking.text = "${arrayMundos[p1].getString("usuarioRanking")}: ${arrayMundos[p1].getInt("puntuacionRanking")} Puntos"
        p0.tipo = arrayMundos[p1].getString("tipo").toString()
        p0.index = p1
        if (idUsuario.getInt("nivel") < arrayMundos[p1].getString("nivel")!!.toInt()) {
            p0.nombreMundo.alpha = 0.5f
        }else{
            p0.nombreMundo.alpha = 1.0f
        }

        val query = ParseQuery.getQuery<ParseObject>("MundosUsuarios")
        query.whereEqualTo("objectIdMundo", arrayMundos[p1].getString("nivel"))
        query.whereEqualTo("tipo", "usuario")
        query.orderByDescending("puntuacion")
        var num = 0
        query.findInBackground { rankingLista, e ->
            if (e == null) {
            }
            for (ranking in rankingLista){
                num += 1
                p0.rankingTexto += "${num} ${ranking.getString("nombre")}: ${ranking.getInt("puntuacion")} \n"
            }

            }


    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var nombreMundo: TextView
        var nombreRanking: TextView
        var rankingBoton: ImageView
        var rankingTexto: String

        var tipo: String
        private var intent2 = Intent(itemView.context, RolActivity::class.java)
        var index: Int

        init {

            // [START initialize_auth]
            val mAuth = FirebaseAuth.getInstance()
            // [END initialize_auth]
            // Check if user is signed in (non-null) and update UI accordingly.
            val currentUser = mAuth.currentUser

            nombreMundo = itemView.findViewById(R.id.nombreMundo)
            nombreRanking = itemView.findViewById(R.id.nombreRanking)
            rankingBoton = itemView.findViewById(R.id.rankingBoton)
            rankingTexto = ""
            tipo = ""
            index = 0

            //Listener para mostrar una alert con los datos del ranking por nivel
            rankingBoton.setOnClickListener {
                val alerta = AlertDialog.Builder(itemView.context)
                alerta.setTitle("Ranking")
                alerta.setMessage(rankingTexto)
                alerta.setPositiveButton("OK") { _, _ ->
                }
                alerta.show()
            }
            //Celda Listener para pasar datos si se seleciona la celda
            itemView.setOnClickListener {
                val progressDialog = ProgressDialog(itemView.context)
                progressDialog.setMessage("Loading...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                if (idUsuario.getInt("nivel") < arrayMundos[index].getString("nivel")!!.toInt()) {
                    Toast.makeText(
                        itemView.context,
                        "Activa este nivel consiguiendo 1000 puntos en el nivel anterior",
                        Toast.LENGTH_LONG
                    ).show()
                } else {

                    if (tipo == "gps") {

                        RolActivity.mundo = arrayMundos[index]
                        RolActivity.objecIdUsuario = idUsuario

                        val query = ParseQuery.getQuery<ParseObject>("MundosUsuarios")

                        query.whereEqualTo("objectIdUsuario", idUsuario.objectId)
                        query.whereEqualTo("objectIdMundo", arrayMundos[index].getString("nivel")!!)
                        query.findInBackground { mundosUsuarios, e ->
                            var usuarioExiste = false
                            var errorBaseDatos = false
                            if (e == null) {
                                //si el registro en la base de datos ya existe abro el mapa
                                if (mundosUsuarios.size > 0) {
                                    usuarioExiste = true
                                    RolActivity.puntos = mundosUsuarios[0]
                                    itemView.context.startActivity(intent2)
                                    progressDialog.cancel()
                                }

                            } else {
                                Log.d("Usuarios", "Error: " + e.message)
                                //Comprobamos si hay conexion a internet0
                                errorBaseDatos = true

                            }
                            //si el registro en la base de datos no existe lo creo y abro el mapa
                            if (!errorBaseDatos && !usuarioExiste) {
                                val mundosUsuario = ParseObject("MundosUsuarios")
                                mundosUsuario.put("objectIdUsuario", idUsuario.objectId)
                                mundosUsuario.put("nivel", 1)
                                mundosUsuario.put("vida", 100)
                                mundosUsuario.put("ataque", 10)
                                mundosUsuario.put("velocidad", 100)
                                mundosUsuario.put("objectIdMundo", arrayMundos[index].getString("nivel")!!)
                                mundosUsuario.put("lat", arrayMundos[index].getDouble("latPuntoCentral"))
                                mundosUsuario.put("lng", arrayMundos[index].getDouble("lngPuntoCentral"))
                                mundosUsuario.put("tipo", "usuario")
                                mundosUsuario.put("icono", imageToFile(R.drawable.puno))
                                mundosUsuario.put("distanciaAtaque", 10)
                                mundosUsuario.put("nombre", currentUser!!.displayName!!)
                                mundosUsuario.put("puntuacion", 0)
                                mundosUsuario.saveInBackground {
                                    RolActivity.puntos = mundosUsuario
                                    itemView.context.startActivity(intent2)
                                    progressDialog.cancel()
                                }
                            }
                        }

                    }
                }
            }

        }
    }

    private fun imageToFile(image: Int): ParseFile {
        val imagenOriginal = BitmapFactory.decodeResource(context.resources, image)
        val bitmap = Bitmap.createScaledBitmap(imagenOriginal, 100, 100, false)
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)

        return ParseFile("imagen.png", stream.toByteArray())
    }
}