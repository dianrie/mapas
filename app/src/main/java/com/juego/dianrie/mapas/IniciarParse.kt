package com.juego.dianrie.mapas

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.widget.Toast
import com.parse.Parse
import com.parse.ParseObject
import com.parse.ParseQuery
import android.content.pm.PackageManager
import android.content.Intent
import android.net.Uri


class IniciarParse : Application() {
    override fun onCreate() {
        super.onCreate()

        // [START BUDDY BASE DE DATOS PARSE]
        //Enable Local Datastore

        Parse.enableLocalDatastore(this)

        Parse.initialize(
            Parse.Configuration.Builder(this)
                .applicationId("ZLRE7lcpvEZElpk00dCQcB23lpnFbOylEMI4Dssc")
                .clientKey("4nSVjgcu1KdUOFqwydub9XqF4wyc9efeyNQjJATn")

                .server("https://parseapi.back4app.com")
                .enableLocalDataStore()
                .build()
        )

    }

}