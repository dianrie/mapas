package com.juego.dianrie.mapas

import org.json.JSONObject
import java.lang.Exception

class Ubicaciones {
    //Propiedades
    //var id:String? = null
    //var poblacion: String? = null
    var nombre: String? = null
    var tipo: String? = null
    var latitud: Double = 0.0
    var longitud: Double = 0.0

    companion object {
        fun jsonAReferencia(referencia:JSONObject): Ubicaciones?{
            try {
                val resultado = Ubicaciones()
                //val geometry = referrencia.getJSONObject("geometry")
                //val location = geometry.getJSONObject("location")
                val latitudLocation = referencia.getString("lat")
                val longitudLocation = referencia.getString("lng")

                resultado.latitud = latitudLocation.toDouble()
                resultado.longitud = longitudLocation.toDouble()

               // val id = referencia.getString("geonameId")
                //resultado.id = id

               // resultado.poblacion = referencia.getString("population")
                resultado.nombre = referencia.getString("name")
             //   resultado.tipo = referencia.getString("typeName")

                return resultado

            }catch (e:Exception){
                e.printStackTrace()
            }

            return null
        }
    }
}