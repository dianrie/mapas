package com.juego.dianrie.mapas

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.animation.AnimationUtils
import com.parse.*
import kotlinx.android.synthetic.main.activity_selecionar_personaje.*
import kotlinx.android.synthetic.main.content_selecionar_personaje.*
import java.io.ByteArrayOutputStream

class SelecionarPersonajeActivity : AppCompatActivity() {
    var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapterSeleccionarPersonaje.ViewHolder>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selecionar_personaje)
        setSupportActionBar(toolbarSeleccionarPersonaje)

        //Pongo seleccione su personaje en el titulo
        //toolbarSeleccionarPersonaje.title = "Seleccione su Personaje"
        this.supportActionBar!!.title = "Seleccione su Personaje"
        cargarDatos()
    }

    private fun cargarReciclerView(arrayAvatar: MutableList<Bitmap>) {
        //RecyclerView -----------------------------------------
        //Inicializamos
        //layoutManager = LinearLayoutManager(this)
        layoutManager = GridLayoutManager(this, 4)
        adapter = RecyclerAdapterSeleccionarPersonaje(arrayAvatar)

        //Asignamos al RecyclerView
        recyclerViewPersonajes.layoutManager = layoutManager
        recyclerViewPersonajes.adapter = adapter
        runLayoutAnimation(recyclerViewPersonajes)
        //------------------------------------------------------
    }

    private fun cargarDatos() {
        val arrayAvatar: MutableList<Bitmap> = mutableListOf()

        val query = ParseQuery.getQuery<ParseObject>("AvatarUsuarios")
        query.findInBackground { listaAvatar, e ->
            if (e == null) {
                if (listaAvatar.count() > 0) {
                    for (avatar in listaAvatar) {
                        //saco la imagen del personaje de la base de datos y la meto en su marcador
                        val applicantResume = avatar.getParseFile("avatar")
                        applicantResume?.getDataInBackground { data, error ->
                            if (error == null) {
                                // data has the bytes for the resume
                                val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
                                val imagenFinal = Bitmap.createScaledBitmap(bmp, 200, 200, false)
                                arrayAvatar.add(imagenFinal)
                                cargarReciclerView(arrayAvatar)
                            } else {
                                // something went wrong
                            }
                        }

                    }

                } else {
                    val avatares: MutableList<Int> =
                        mutableListOf(
                            R.drawable.superhero001,
                            R.drawable.superhero002,
                            R.drawable.superhero003,
                            R.drawable.superhero004,
                            R.drawable.superhero005,
                            R.drawable.superhero006,
                            R.drawable.superhero007,
                            R.drawable.superhero008,
                            R.drawable.superhero009,
                            R.drawable.superhero010,
                            R.drawable.superhero011,
                            R.drawable.superhero012,
                            R.drawable.superhero013,
                            R.drawable.superhero014,
                            R.drawable.superhero015,
                            R.drawable.superhero016,
                            R.drawable.superhero017,
                            R.drawable.superhero018,
                            R.drawable.superhero019,
                            R.drawable.superhero020,
                            R.drawable.superhero021,
                            R.drawable.superhero022,
                            R.drawable.superhero023,
                            R.drawable.superhero024,
                            R.drawable.superhero025,
                            R.drawable.superhero026,
                            R.drawable.superhero027,
                            R.drawable.superhero028,
                            R.drawable.superhero029,
                            R.drawable.superhero030,
                            R.drawable.superhero031,
                            R.drawable.superhero032,
                            R.drawable.superhero033,
                            R.drawable.superhero034,
                            R.drawable.superhero035,
                            R.drawable.superhero036,
                            R.drawable.superhero037,
                            R.drawable.superhero038,
                            R.drawable.superhero039,
                            R.drawable.superhero040,
                            R.drawable.superhero041,
                            R.drawable.superhero042,
                            R.drawable.superhero043,
                            R.drawable.superhero044
                        )
                    var count = 0
                    for (i in avatares) {
                        count += 1
                        val avatar = ParseObject("AvatarUsuarios")
                        avatar.put("avatar", imageToFile(i))

                        if (count == avatares.count()) {
                            avatar.saveInBackground { cargarDatos() }
                        } else {
                            avatar.saveInBackground {}
                        }

                    }
                }
            } else {

            }
        }
    }

    private fun imageToFile(image: Int): ParseFile {
        val imagenOriginal = BitmapFactory.decodeResource(resources, image)
        val bitmap = Bitmap.createScaledBitmap(imagenOriginal, 300, 300, false)
        //val bitmap = getResizedBitmap(imagenOriginal,100,100)
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)

        return ParseFile("imagen.png", stream.toByteArray())

    }

    private fun runLayoutAnimation(recyclerView: RecyclerView) {
        val context = recyclerView.context
        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        recyclerView.layoutAnimation = controller
        recyclerView.adapter!!.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
    }
/*
    override fun finish() {
        //Intent de retorno
        val intentRetorno = Intent()
        //intentRetorno.putExtra("mensajeRetorno","Holita enviado desde ActivityB")
        setResult(Activity.RESULT_OK,intentRetorno)
        super.finish()
    }
*/
}
