package com.juego.dianrie.mapas


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.parse.ParseFile
import com.parse.ParseObject
import com.parse.ParseQuery
import kotlinx.android.synthetic.main.activity_seleccionar_mundo.*
import kotlinx.android.synthetic.main.content_seleccionar_mundo.*
import java.io.ByteArrayOutputStream

class SeleccionarMundoActivity : AppCompatActivity() {

    companion object {
        var idUsuario: ParseObject? = null
    }
    var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapterSeleccionarMundo.ViewHolder>? = null
    private var errorBaseDatos = false
    private var currentUser: FirebaseUser? = null
    private var primeraVez = true

    private var arrayParseObjectMundos: MutableList<ParseObject>? = null

    private lateinit var progressDialog : ProgressDialog

    // [START declare_auth]
    private lateinit var mAuth: FirebaseAuth

    override fun onResume() {
        super.onResume()
        if (primeraVez) {
            primeraVez = false
        } else {
            cargarReciclerView(arrayParseObjectMundos!!)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seleccionar_mundo)
        setSupportActionBar(toolbarMundos)

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance()
        // [END initialize_auth]
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.currentUser

        //Pongo seleccione su personaje en el titulo
        //toolbarMundos.title = ""
        this.supportActionBar!!.title = "Seleccione un Mundo"
        //consultarDatos()
        cargarDatosParse()


        //Pulsando el boton log out
        salirBoton.setOnClickListener {
            // [START auth_fui_signout]
            AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    finish()
                    val intent = Intent(this, FirebaseUIActivity::class.java)
                    startActivity(intent)
                }
        }
        //Pulsando el boton avatar
        avatar2Boton.setOnClickListener {
            val intent = Intent(this, SelecionarPersonajeActivity::class.java)
            startActivity(intent)
        }


    }

    private fun cargarReciclerView(arrayAvatar: MutableList<ParseObject>) {
        //RecyclerView -----------------------------------------
        //Inicializamos
        layoutManager = LinearLayoutManager(this)
        adapter = RecyclerAdapterSeleccionarMundo(arrayAvatar, idUsuario!!, this)

        //Asignamos al RecyclerView
        recyclerViewMundos.layoutManager = layoutManager
        recyclerViewMundos.adapter = adapter
        runLayoutAnimation(recyclerViewMundos)
        //------------------------------------------------------
    }

    private fun consultarDatos() {

        val query = ParseQuery.getQuery<ParseObject>("Mundos")
        query.orderByAscending("nivel")
        query.findInBackground { mundos1, e ->
            if (mundos1.count() > 0) {
                if (e == null) {
                    //Saco los Mundos y creo un array de objetos parse para pasarselos al adapter
                    arrayParseObjectMundos = mundos1
                    cargarReciclerView(arrayParseObjectMundos!!)
                    progressDialog.cancel()
                } else {

                }
            } else {
                val mundos: MutableList<Mundo> =
                    mutableListOf(
                        Mundo("(Madrid)", "gps", 40.4168545, -3.7036666, "1","",0),
                        Mundo("(Albox)", "gps", 37.388566, -2.1463984, "2","",0),
                        Mundo("(Murcia)", "gps", 37.9929659, -1.1322225, "3","",0)
                    )
                var count = 0
                for (i in mundos) {
                    count += 1
                    val mundo = ParseObject("Mundos")
                    mundo.put("nombre", i.nombre)
                    mundo.put("tipo", i.tipo)
                    mundo.put("latPuntoCentral", i.latPuntoCentral)
                    mundo.put("lngPuntoCentral", i.lngPuntoCentral)
                    mundo.put("nivel", i.nivel)
                    mundo.put("usuarioRanking", i.usuarioRanking)
                    mundo.put("puntuacionRanking", i.puntuacionRanqing)

                    if (count == mundos.count()) {
                        mundo.saveInBackground { consultarDatos() }
                    } else {
                        mundo.saveInBackground {}
                    }
                }
                val mundosUsuario: MutableList<MundosUsuario> =
                    mutableListOf(
                        MundosUsuario("torero","enemigo",2,1,10,imageToFile(R.drawable.torero),"2",50,1,100),
                        MundosUsuario("slime","enemigo",1,1,10,imageToFile(R.drawable.slime),"1",30,1,100),
                        MundosUsuario("ZombieBebe","enemigo",2,1,10,imageToFile(R.drawable.zombibebe),"2",20,100,100),
                        MundosUsuario("Zombie","enemigo",2,1,10,imageToFile(R.drawable.zombie),"1",20,1,100),
                        MundosUsuario("Zombie","enemigo",5,1,10,imageToFile(R.drawable.zombie),"3",20,5,150),
                        MundosUsuario("Franky","enemigo",10,1,20,imageToFile(R.drawable.frankenstein),"3",40,1,200),
                        MundosUsuario("Asesinillo","enemigo",3,1,200,imageToFile(R.drawable.superhero006),"3",60,10,30),
                        MundosUsuario("espada","objeto",1,0,100,imageToFile(R.drawable.espada),"1",100,10,100),
                        MundosUsuario("bomba","objeto",1,0,50,imageToFile(R.drawable.bomba),"2",100,30,100),
                        MundosUsuario("Rifle","objeto",1,0,500,imageToFile(R.drawable.rifle),"3",100,20,100),
                        MundosUsuario("Pizzeria","edificio",1,0,0,imageToFile(R.drawable.pizzeria),"1",100,0,100),
                        MundosUsuario("Hospital","edificio",1,0,0,imageToFile(R.drawable.hospital),"1",100,0,100)
                    )
                for(i in mundosUsuario){
                    val mundosUsuario1 = ParseObject("MundosUsuarios")
                    mundosUsuario1.put("nivel",i.nivel )
                    mundosUsuario1.put("vida", i.vida)
                    mundosUsuario1.put("ataque", i.ataque)
                    mundosUsuario1.put("velocidad", i.velocidad)
                    mundosUsuario1.put("objectIdMundo", i.objectIdMundo)
                    mundosUsuario1.put("tipo", i.tipo)
                    mundosUsuario1.put("icono", i.icono)
                    mundosUsuario1.put("distanciaAtaque", i.distanciaAtaque)
                    mundosUsuario1.put("nombre", i.nombre)
                    mundosUsuario1.put("puntuacion", i.puntuacion)
                    mundosUsuario1.saveInBackground ()
                }
            }
        }
    }

    private fun cargarDatosParse() {
        val query = ParseQuery.getQuery<ParseObject>("Usuarios")
        query.findInBackground { listaUsuarios, e ->
            if (e == null) {
                errorBaseDatos = false
                for (usuario in listaUsuarios) {
                    val idUsu = usuario.getString("idUsuario")
                    if (idUsu == currentUser!!.uid) {
                        idUsuario = usuario

                    }
                }
            } else {
                Log.d("Usuarios", "Error: " + e.message)
                //Comprobamos si hay conexion a internet0
                if (!compruebaConexion(this)) {
                    Toast.makeText(this, "No tiene conexión a internet", Toast.LENGTH_LONG).show()
                    finish()
                }
                Toast.makeText(this, "Error base de datos: " + e.message, Toast.LENGTH_LONG).show()
                errorBaseDatos = true

            }
            //Si es la primera vez que juegas se crea el personaje en la base de datos
            //Se crea una mascota y se guarda en la base de datos


            if (idUsuario == null) {

                val usuario = ParseObject("Usuarios")
                usuario.put("idUsuario", currentUser!!.uid)
                usuario.put("nombre", currentUser!!.displayName!!)
                usuario.put("lat", 0.0)
                // usuario.put("imagen", personaje!!.imagen)
                usuario.put("lng", 0.0)
                usuario.put("casaLat", 0.0)
                usuario.put("casaLng", 0.0)
                usuario.put("avatar", imageToFile(R.drawable.otropersonaje))
                usuario.put("nivel", 1)

                usuario.saveInBackground {
                    query.whereEqualTo("idUsuario", currentUser!!.uid)
                    query.findInBackground { listaUsuarios, e ->
                        if (e == null) {
                            idUsuario = listaUsuarios[0]
                            consultarDatos()
                            for (i in 0..3) {
                                val mascota = ParseObject("Mascotas")
                                if (i==0){
                                    mascota.put("objectIdUsuario", idUsuario!!.objectId)
                                    mascota.put("icono", imageToFile(R.drawable.zangano))
                                    mascota.put("nombre", "Robot")
                                    mascota.put("posicion", 0)
                                    mascota.put("imagen", R.drawable.zangano)
                                    mascota.put("velocidad", 50)
                                    mascota.put("ataque", 10)
                                    mascota.put("nivel", 1)
                                    mascota.put("vida", 100)
                                }else {

                                    mascota.put("objectIdUsuario", idUsuario!!.objectId)
                                    mascota.put("nombre", "")
                                    mascota.put("posicion", i)
                                    // mascota.put("imagen", 0)
                                    mascota.put("velocidad", 0)
                                    mascota.put("ataque", 0)
                                    mascota.put("nivel", 0)
                                }
                                mascota.saveInBackground()
                            }
                        } else {
                            Log.i("error", "error Mascotas")
                        }
                    }

                }
                val intent = Intent(this, SelecionarPersonajeActivity::class.java)
                startActivity(intent)
                //startActivityForResult(intent, 111)
            }else{
                consultarDatos()
            }


        }


    }

    private fun compruebaConexion(context: Context): Boolean {
        var connected = false
        val connec = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        // Recupera todas las redes (tanto móviles como wifi)
        val redes = connec.allNetworkInfo
        for (i in redes.indices) {
            // Si alguna red tiene conexión, se devuelve true
            if (redes[i].state === NetworkInfo.State.CONNECTED) {
                connected = true
            }
        }
        return connected
    }

    private fun imageToFile(image: Int): ParseFile {
        val imagenOriginal = BitmapFactory.decodeResource(resources, image)
        val bitmap = Bitmap.createScaledBitmap(imagenOriginal, 300, 300, false)
        //val bitmap = getResizedBitmap(imagenOriginal,100,100)
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)

        return ParseFile("imagen.png", stream.toByteArray())

    }

    fun getResizedBitmap(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap {
        val resizedBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888)
        val scaleX = newWidth / bitmap.width
        val scaleY = newHeight / bitmap.height
        val pivotX = 0f
        val pivotY = 0f
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(scaleX.toFloat(), scaleY.toFloat(), pivotX, pivotY)
        val canvas = Canvas(resizedBitmap)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bitmap, 0f, 0f, Paint(Paint.FILTER_BITMAP_FLAG))

        return resizedBitmap
    }
    private var backpressed:Long = 0
    override fun onBackPressed() {
        if (backpressed + 2000 > System.currentTimeMillis())
            super.onBackPressed()
        else
            Toast.makeText(this, "Pulse otra vez para salir", Toast.LENGTH_SHORT).show()
        backpressed = System.currentTimeMillis()
    }
    private fun runLayoutAnimation(recyclerView:RecyclerView) {
        val context = recyclerView.context
        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        recyclerView.layoutAnimation = controller
        recyclerView.adapter!!.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
    }
}

class Mundo(
    var nombre: String,
    var tipo: String,
    var latPuntoCentral: Double,
    var lngPuntoCentral: Double,
    var nivel: String,
    var usuarioRanking: String,
    var puntuacionRanqing: Int
)
class MundosUsuario(
    var nombre: String,
    var tipo: String,
    var nivel: Int,
    var puntuacion: Int,
    var distanciaAtaque: Int,
    var icono: ParseFile,
    var objectIdMundo: String,
    var velocidad: Int,
    var ataque: Int,
    var vida: Int
)