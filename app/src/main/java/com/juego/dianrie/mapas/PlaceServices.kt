package com.juego.dianrie.mapas

import android.util.Log
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.URL

class PlaceServices {


    fun buscarRestaurantes(latitud: Double, longitud: Double): ArrayList<Ubicaciones?>{
    val url = crearURL(latitud, longitud)
        Log.i("TAG","Petición a $url")
        val arrayRestaurantes = ArrayList<Ubicaciones?>()
        try {
            val json = JSONObject(getJSON(url))
            val array = json.getJSONArray("geonames")
            //val array = json.getJSONArray("poi")

            for (i in 0 until array.length()){
                val restauranteCercano = Ubicaciones.jsonAReferencia(array.get(i) as JSONObject)
                arrayRestaurantes.add(restauranteCercano)
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
        return arrayRestaurantes
    }

    private fun crearURL (latitud: Double, longitud: Double): String {
        //val url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=123+main+street&location=42.3675294,-71.186966&radius=10000&key=AIzaSyCf6gu3sa_s6dDNEHFJPeErlQI1Wa7lRig"
       // return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?&location=$latitud,$longitud&radius=1000&keyword=$keyword&sensor=false&key=AIzaSyAx6NkvX4_0huJaDeIyDd_Jve6oW8p2m0E"
        //return "http://api.geonames.org/findNearbyPlaceNameJSON?lat=$latitud&lng=$longitud&radius=1&username=dianrie"
        //return "http://api.geonames.org/findNearbyPOIsOSMJSON?lat=$latitud&lng=$longitud&radius=1&maxRows=50&username=dianrie"
        return "http://api.geonames.org/findNearbyPlaceNameJSON?lat=$latitud&lng=$longitud&cities=cities1000&radius=100&username=dianrie"
        //return "http://api.geonames.org/findNearbyPlaceNameJSON?lat=$latitud&lng=$longitud&radius=10&username=dianrie"
    }

    private fun getJSON(url:String): String {
        val contenidoRespuesta = StringBuilder()
        try {
            val urlConnection = URL(url).openConnection()
            val bufferedReader = BufferedReader(InputStreamReader(urlConnection.getInputStream()))
            var line: String?
            do {
                line = bufferedReader.readLine()
                contenidoRespuesta.append(line + "\n")
            }while ( line != null)
            bufferedReader.close()
        }catch ( e: Exception) {
            e.printStackTrace()
        }
        return contenidoRespuesta.toString()
    }

}