package com.juego.dianrie.mapas


import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build


class SoundManager(var context: Context) {

    private lateinit var soundPool: SoundPool
    private lateinit var sm: IntArray
    fun initSound() {
        val maxStreams = 1
        val mContext = context
        soundPool = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SoundPool.Builder()
                .setMaxStreams(maxStreams)
                .build()
        } else {
            SoundPool(maxStreams, AudioManager.STREAM_MUSIC, 0)
        }
        sm = IntArray(7)
        // fill your sounds
        sm[0] = soundPool.load(mContext, R.raw.golpe, 1)
        sm[1] = soundPool.load(mContext, R.raw.telemetria, 1)
        sm[2] = soundPool.load(mContext, R.raw.robot, 1)
        sm[3] = soundPool.load(mContext, R.raw.explosion, 1)
        sm[4] = soundPool.load(mContext, R.raw.muertemostruo, 1)
        sm[5] = soundPool.load(mContext, R.raw.objeto, 1)
        sm[6] = soundPool.load(mContext, R.raw.laser, 1)
    }

    internal fun playSound(sound: Int) {
        soundPool.play(sm[sound], 1f, 1f, 1, 0, 1f)
    }

    fun cleanUpIfEnd() {
        soundPool.release()
    }
}

